import netsquid as ns
import unittest

from netsquid.qubits.ketstates import BellIndex
from netsquid.nodes import Node

from netsquid_driver.driver import Driver
from netsquid_entanglementtracker.local_link_tracker import LocalLinkTracker
from netsquid_entanglementtracker.cutoff_service import CutoffService
from netsquid_driver.entanglement_tracker_service import LinkIdentifier, LinkStatus, \
    EntanglementTrackerService


class TestCutoffService(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        node_id = 0
        self.node = Node("test_cutoff_service_node", ID=node_id)
        self.link1 = LinkIdentifier(node_ids=frozenset({node_id, node_id + 1}), pair_identifier=0)

    def set_driver_and_local_link_tracker(self):
        """This helper method sets up a driver, adds LocalLinkTracker to the driver, and registers link1."""
        # set up driver
        self.driver = Driver(name="test_driver")
        self.node.add_subcomponent(self.driver)
        self.node.driver = self.driver

        # set up LocalLinkTracker and register link1 so it can be later discarded
        self.tracker = LocalLinkTracker(node=self.node, name="test_local_link_tracker")
        self.node.driver.add_service(EntanglementTrackerService, self.tracker)
        self.node.driver[EntanglementTrackerService].register_new_link(link_identifier=self.link1,
                                                                       bell_index=BellIndex.B01, mem_pos=0,
                                                                       times_of_birth=0)

    def test_discard_link(self):
        self.set_driver_and_local_link_tracker()
        assert self.tracker.get_status(self.link1) == LinkStatus.AVAILABLE

        cutoff_service = CutoffService(node=self.node, name="test_cutoff_service")
        cutoff_service.discard_link(self.link1)

        assert cutoff_service.is_connected
        assert self.tracker.get_status(self.link1) == LinkStatus.DISCARDED

    def test_check_for_entanglement_tracker(self):
        """CutoffService is not connected when the Node does not have an EntanglementTracker."""
        cutoff_service = CutoffService(node=self.node, name="test_cutoff_service")
        assert not cutoff_service.is_connected
