import unittest
from netsquid.nodes.node import Node
from netsquid.qubits.ketstates import BellIndex
from netsquid_driver.entanglement_tracker_service import LinkIdentifier, EntanglementInfo, \
    EntanglementTrackerService, BellStateEntanglementInfo, ReqDiscardUpdate, ReqNewLinkUpdate, ReqSwapUpdate, \
    ReqMeasureUpdate


class MockEntanglementTracker(EntanglementTrackerService):

    def __init__(self, node):
        super().__init__(node=node, name="mock_entanglement_tracker")
        self.performed_link_update = False
        self.performed_discard_update = False
        self.performed_measurement_update = True
        self.performed_swap_update = False

    def register_new_link(self, link_identifier, bell_index, mem_pos=None, times_of_birth=None):
        self.performed_link_update = True

    def register_discard(self, link_identifier, node_id=None, time_of_discard=None):
        self.performed_discard_update = True

    def register_measurement(self, link_identifier, node_id=None, time_of_measurement=None):
        self.performed_measurement_update = True

    def register_swap(self, link_identifier_1, link_identifier_2, bell_index, time_of_swap=None):
        self.performed_swap_update = True

    def get_link(self, mem_pos):
        return LinkIdentifier(frozenset({self.node.ID, self.node.ID + 1}), 40)

    @property
    def num_available_links(self):
        return 0

    def register_move(self, old_mem_pos, new_mem_pos):
        pass

    def get_status(self, link_identifier):
        return None

    def get_mem_pos(self, link_identifier):
        return None

    def get_links_by_status(self, link_status):
        return []

    def get_local_link_lifetime(self, link_identifier):
        return 0

    def get_entanglement_info_of_link(self, link_identifier):
        return EntanglementInfo(link_identifier, BellIndex.B00, None)

    def get_bell_index(self, link_identifier):
        return None

    def get_entanglement_info_between_nodes(self, node_ids, other_nodes_allowed=False):
        return []

    def await_entanglement(self, node_ids, link_identifiers=set(), other_nodes_allowed=False):
        yield


class TestEntanglementTrackerService(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.node = Node("test_node")
        cls.link1 = LinkIdentifier(node_ids=frozenset({cls.node.ID, cls.node.ID + 1}),
                                   pair_identifier=0)
        cls.link2 = LinkIdentifier(node_ids=frozenset({cls.node.ID, cls.node.ID + 2}),
                                   pair_identifier=5)

    def setUp(self):
        self.tracker = MockEntanglementTracker(node=self.node)

    def test_req_new_link(self):
        req = ReqNewLinkUpdate(link_identifier=self.link1,
                               bell_index=BellIndex.B00)
        self.tracker.put(req)
        assert self.tracker.performed_link_update

    def test_req_discard(self):
        req = ReqDiscardUpdate(link_identifier=self.link1,
                               node_id=100)
        self.tracker.put(req)
        assert self.tracker.performed_discard_update

    def test_req_measure(self):
        req = ReqMeasureUpdate(link_identifier=self.link1,
                               node_id=93)
        self.tracker.put(req)
        assert self.tracker.performed_measurement_update

    def test_register_new_local_link(self):
        self.tracker.register_new_local_link(remote_node_id=self.node.ID + 1,
                                             bell_index=BellIndex.B10)
        assert self.tracker.performed_link_update

    def test_meas_mem_pos(self):
        self.tracker.register_local_measurement_mem_pos(mem_pos=10)
        assert self.tracker.performed_measurement_update

    def test_discard_mem_pos(self):
        self.tracker.register_local_discard_mem_pos(mem_pos=432)
        assert self.tracker.performed_discard_update

    def test_req_swap(self):
        req = ReqSwapUpdate(link_identifier_1=self.link1,
                            link_identifier_2=self.link2,
                            bell_index=BellIndex.B11)
        self.tracker.put(req)
        assert self.tracker.performed_swap_update

    def test_swap_mem_pos(self):
        self.tracker.register_local_swap_mem_pos(mem_pos_1=20, mem_pos_2=0, bell_index=BellIndex.B01)
        assert self.tracker.performed_swap_update


class TestLinkIdentifier(unittest.TestCase):
    def test_link_identifier(self):

        link = LinkIdentifier(frozenset([0, 1]), 0)

        # test link identifiers are considered equal if the nodes are permuted
        assert link == LinkIdentifier(frozenset([1, 0]), 0)

        # assert link identifiers are hashable
        test_dict = {link: 10}
        assert test_dict[link] == 10

        with self.assertRaises(TypeError):
            LinkIdentifier({0, 1}, 0)


class TestEntanglementInfo(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.link_identifier = LinkIdentifier(node_ids=frozenset({0, 1}), pair_identifier=100)
        cls.bell_index = BellIndex.B00
        cls.time_of_birth = 600

    def setUp(self) -> None:
        self.entanglement_info = EntanglementInfo(link_identifier=self.link_identifier,
                                                  bell_index=self.bell_index,
                                                  times_of_birth=self.time_of_birth)

    def test_initializing(self):
        assert self.entanglement_info.elementary
        assert self.entanglement_info.intact
        assert self.entanglement_info.elementary_links == {self.link_identifier}
        assert self.entanglement_info.entangled_nodes == self.link_identifier.node_ids
        assert self.entanglement_info.get_link_bell_index(self.link_identifier) == self.bell_index
        for node_id in self.link_identifier.node_ids:
            assert (self.entanglement_info.get_time_of_birth(link_identifier=self.link_identifier, node_id=node_id)
                    == self.time_of_birth)
            assert (self.entanglement_info.get_time_of_death(link_identifier=self.link_identifier, node_id=node_id)
                    is None)

    def test_set_intact(self):
        self.entanglement_info.intact = False
        assert not self.entanglement_info.intact
        self.entanglement_info.intact = True
        assert self.entanglement_info.intact

    def test_unentangle_qubit(self):
        node_ids = tuple(self.link_identifier.node_ids)
        node_measured_id = node_ids[0]
        node_kept_id = node_ids[1]
        time_of_unentangling = 900
        self.entanglement_info.unentangle_qubit(node_id=node_measured_id, link_identifier=self.link_identifier,
                                                time_of_unentangling=time_of_unentangling)
        assert self.entanglement_info.entangled_nodes == {node_kept_id}
        assert (self.entanglement_info.get_time_of_death(node_id=node_measured_id, link_identifier=self.link_identifier)
                == time_of_unentangling)

    def test_set_link_bell_index(self):
        new_bell_index = BellIndex.B11
        self.entanglement_info.set_link_bell_index(link_identifier=self.link_identifier,
                                                   bell_index=new_bell_index)
        assert self.entanglement_info.get_link_bell_index(self.link_identifier) == new_bell_index
        entanglement_info = EntanglementInfo(link_identifier=self.link_identifier,
                                             bell_index=None,
                                             times_of_birth=self.time_of_birth)
        entanglement_info.set_link_bell_index(link_identifier=self.link_identifier,
                                              bell_index=new_bell_index)
        assert entanglement_info.get_link_bell_index(self.link_identifier) == new_bell_index

    def test_set_time_of_birth(self):
        time_of_birth = 831
        node_id = tuple(self.link_identifier.node_ids)[0]
        self.entanglement_info.set_time_of_birth(node_id=node_id,
                                                 link_identifier=self.link_identifier,
                                                 time_of_birth=time_of_birth)
        assert self.entanglement_info.get_time_of_birth(node_id=node_id,
                                                        link_identifier=self.link_identifier) == time_of_birth

    def test_set_time_of_death(self):
        time_of_death = 831
        node_id = tuple(self.link_identifier.node_ids)[0]
        self.entanglement_info.set_time_of_death(node_id=node_id,
                                                 link_identifier=self.link_identifier,
                                                 time_of_death=time_of_death)
        assert self.entanglement_info.get_time_of_death(node_id=node_id,
                                                        link_identifier=self.link_identifier) == time_of_death

    def test_merge(self):
        other_link_identifier = LinkIdentifier(node_ids=frozenset({10, 11}), pair_identifier=42)
        other_bell_index = BellIndex.B11
        other_time_of_birth = 1400
        other_entanglement_info = EntanglementInfo(link_identifier=other_link_identifier,
                                                   bell_index=other_bell_index,
                                                   times_of_birth=other_time_of_birth)
        new_entanglement_info = self.entanglement_info.merge(other_entanglement_info)
        assert not new_entanglement_info.elementary
        assert self.link_identifier in new_entanglement_info.elementary_links
        assert other_link_identifier in new_entanglement_info.elementary_links
        assert new_entanglement_info.get_link_bell_index(self.link_identifier) == self.bell_index
        assert new_entanglement_info.get_link_bell_index(other_link_identifier) == other_bell_index
        for node_id in self.link_identifier.node_ids | other_link_identifier.node_ids:
            assert node_id in new_entanglement_info.entangled_nodes
            if node_id in self.link_identifier.node_ids:
                assert (new_entanglement_info.get_time_of_birth(link_identifier=self.link_identifier, node_id=node_id)
                        == self.time_of_birth)
                assert (new_entanglement_info.get_time_of_death(link_identifier=self.link_identifier, node_id=node_id)
                        is None)
                with self.assertRaises(ValueError):
                    new_entanglement_info.get_time_of_birth(link_identifier=other_link_identifier, node_id=node_id)
            else:
                assert (new_entanglement_info.get_time_of_birth(link_identifier=other_link_identifier, node_id=node_id)
                        == other_time_of_birth)
                assert (new_entanglement_info.get_time_of_death(link_identifier=other_link_identifier, node_id=node_id)
                        is None)
                with self.assertRaises(ValueError):
                    new_entanglement_info.get_time_of_birth(link_identifier=self.link_identifier, node_id=node_id)

    def test_unequal_times_of_birth(self):
        node_ids = tuple(self.link_identifier.node_ids)
        time_of_birth_2 = 1400
        entanglement_info = EntanglementInfo(link_identifier=self.link_identifier,
                                             bell_index=self.bell_index,
                                             times_of_birth={
                                                 node_ids[0]: self.time_of_birth,
                                                 node_ids[1]: time_of_birth_2
                                             }
                                             )
        assert (entanglement_info.get_time_of_birth(node_id=node_ids[0], link_identifier=self.link_identifier) ==
                self.time_of_birth)
        assert (entanglement_info.get_time_of_birth(node_id=node_ids[1], link_identifier=self.link_identifier) ==
                time_of_birth_2)

    def test_entangled_qubits(self):
        assert set(self.entanglement_info._entangled_qubits) == set(self.entanglement_info._qubit_identifiers)

    def test_get_qubit_data(self):
        with self.assertRaises(KeyError):
            self.entanglement_info._get_qubit_data(node_id=100,
                                                   link_identifier=LinkIdentifier(frozenset({100, 200}), 1))

    def test_are_qubits_alive(self):
        assert self.entanglement_info.has_alive_qubits
        self.entanglement_info.set_time_of_death(node_id=0, link_identifier=self.link_identifier, time_of_death=0)
        assert self.entanglement_info.has_alive_qubits
        self.entanglement_info.unentangle_qubit(node_id=1, link_identifier=self.link_identifier, time_of_unentangling=1)
        assert not self.entanglement_info.has_alive_qubits


class TestBellStateEntanglementInfo(unittest.TestCase):

    def setUp(self) -> None:
        self.link_identifier_1 = LinkIdentifier(frozenset({0, 1}), 23)
        self.link_identifier_2 = LinkIdentifier(frozenset({1, 2}), 66)
        self.time_of_birth_1 = 0
        self.time_of_birth_2 = 0
        self.time_of_swap = 100

    def swap(self, link_bell_index_1, link_bell_index_2, swap_bell_index):
        entanglement_info_1 = BellStateEntanglementInfo(link_identifier=self.link_identifier_1,
                                                        bell_index=link_bell_index_1,
                                                        times_of_birth=self.time_of_birth_1)
        entanglement_info_2 = BellStateEntanglementInfo(link_identifier=self.link_identifier_2,
                                                        bell_index=link_bell_index_2,
                                                        times_of_birth=self.time_of_birth_2)
        return entanglement_info_1.merge_by_swap(other_entanglement_info=entanglement_info_2,
                                                 link_identifier_1=self.link_identifier_1,
                                                 link_identifier_2=self.link_identifier_2,
                                                 bell_index=swap_bell_index,
                                                 time_of_swap=self.time_of_swap)

    def test_no_corrections(self):
        entanglement_info = self.swap(link_bell_index_1=BellIndex.B00,
                                      link_bell_index_2=BellIndex.B00,
                                      swap_bell_index=BellIndex.B00)
        assert entanglement_info.entangled_nodes == {0, 2}
        assert entanglement_info.get_time_of_death(node_id=0, link_identifier=self.link_identifier_1) is None
        assert entanglement_info.get_time_of_death(node_id=2, link_identifier=self.link_identifier_2) is None
        assert (entanglement_info.get_time_of_death(node_id=1, link_identifier=self.link_identifier_1)
                == self.time_of_swap)
        assert (entanglement_info.get_time_of_death(node_id=1, link_identifier=self.link_identifier_2)
                == self.time_of_swap)
        assert entanglement_info.swap_bell_index(self.link_identifier_1, self.link_identifier_2) == BellIndex.B00
        assert entanglement_info.bell_index == BellIndex.B00

    def test_deviating_link_bell_index(self):
        entanglement_info = self.swap(link_bell_index_1=BellIndex.B01,
                                      link_bell_index_2=BellIndex.B00,
                                      swap_bell_index=BellIndex.B00)
        assert entanglement_info.bell_index == BellIndex.B01

    def test_deviating_swap_bell_index(self):
        entanglement_info = self.swap(link_bell_index_1=BellIndex.B00,
                                      link_bell_index_2=BellIndex.B00,
                                      swap_bell_index=BellIndex.B10)
        assert entanglement_info.bell_index == BellIndex.B10

    def test_missing_link_bell_index(self):
        entanglement_info = self.swap(link_bell_index_1=BellIndex.B01,
                                      link_bell_index_2=None,
                                      swap_bell_index=BellIndex.B00)
        assert entanglement_info.bell_index is None

    def test_missing_swap_bell_index(self):
        entanglement_info = self.swap(link_bell_index_1=BellIndex.B01,
                                      link_bell_index_2=BellIndex.B00,
                                      swap_bell_index=None)
        assert entanglement_info.bell_index is None

    def test_two_x_two_z_corrections(self):
        entanglement_info = self.swap(link_bell_index_1=BellIndex.B10,
                                      link_bell_index_2=BellIndex.B11,
                                      swap_bell_index=BellIndex.B01)
        assert entanglement_info.bell_index == BellIndex.B00

    def test_maximum_corrections(self):
        entanglement_info = self.swap(link_bell_index_1=BellIndex.B11,
                                      link_bell_index_2=BellIndex.B11,
                                      swap_bell_index=BellIndex.B11)
        assert entanglement_info.bell_index == BellIndex.B11
