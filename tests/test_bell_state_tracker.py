import unittest
from typing import List

import netsquid as ns
from netsquid.nodes.node import Node
from netsquid.protocols import NodeProtocol
from netsquid.qubits.ketstates import BellIndex
from netsquid_driver.driver import Driver
from netsquid_entanglementtracker.bell_state_tracker import BellStateTracker
from netsquid_driver.entanglement_tracker_service import LinkIdentifier, BellStateEntanglementInfo, \
    LinkStatus, ReqNewLinkUpdate, ReqSwapUpdate, ReqDiscardUpdate, EntanglementTrackerService, ReqMeasureUpdate
from netsquid_physlayer.classical_connection import ClassicalConnection
from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest


class TestBellStateTrackerWithoutUntracking(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.num_nodes = 4
        cls.nodes = [Node(f"node_{i}", ID=i, port_names=["A", "B"]) for i in range(cls.num_nodes)]
        cls.node = cls.nodes[1]
        cls.link_identifiers = [LinkIdentifier(node_ids=frozenset([cls.nodes[i].ID, cls.nodes[i + 1].ID]),
                                               pair_identifier=0)
                                for i in range(cls.num_nodes - 1)]
        cls.local_link_1 = cls.link_identifiers[0]
        cls.local_bell_index_1 = BellIndex.B01
        cls.local_link_2 = cls.link_identifiers[1]
        cls.local_bell_index_2 = BellIndex.B10
        cls.nonlocal_link = cls.link_identifiers[2]
        cls.nonlocal_bell_index = BellIndex.B11
        cls.swap_bell_index = BellIndex.B00

    def setUp(self):
        ns.sim_reset()
        self.set_up_node()
        for port in self.node.ports.values():
            port.bind_output_handler(self.register_outgoing_requests)
        self.output_messages = []
        self.message_filter = None

    def set_up_tracker(self):
        self.tracker = BellStateTracker(node=self.node, name="test_bell_state_tracker",
                                        untrack_stale_entanglement=False)

    def set_up_node(self):
        self.node.driver = Driver("driver")
        self.set_up_tracker()
        self.node.driver.add_service(EntanglementTrackerService, self.tracker)

        local_forwarding_table = _calculate_line_forwarding_table(self.nodes)[self.node.name]
        routing_service = ClassicalRoutingService(self.node, local_routing_table=local_forwarding_table)
        self.node.driver.add_service(ClassicalRoutingService, routing_service)

    def register_outgoing_requests(self, message):
        for remote_service_request in message.items:
            assert isinstance(remote_service_request, RemoteServiceRequest)
            assert remote_service_request.origin == self.node.name
            has_downstream_targets = remote_service_request.targets == [self.nodes[0].name]
            has_upstream_targets = remote_service_request.targets == [self.nodes[2].name, self.nodes[3].name]
            assert has_downstream_targets or has_upstream_targets
            assert issubclass(remote_service_request.service, EntanglementTrackerService)

            if self.message_filter is None or isinstance(remote_service_request.request, self.message_filter):
                self.output_messages.append(remote_service_request)

    def test_nothing_registered(self):
        for link in self.link_identifiers:
            assert self.tracker.get_entanglement_info_of_link(link) is None
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True)
        ns.sim_run()
        assert not self.output_messages

    def test_register_new_link_local(self):
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1,
                                       times_of_birth={self.node.ID: 42,
                                                       self.nodes[0].ID: 69})
        assert self.tracker.get_local_link_lifetime(self.local_link_1) == ns.sim_time() - 42
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert entanglement_info.get_time_of_birth(node_id=self.node.ID, link_identifier=self.local_link_1) == 42
        assert entanglement_info.get_time_of_birth(node_id=self.nodes[0].ID, link_identifier=self.local_link_1) == 69
        assert entanglement_info.get_link_bell_index(self.local_link_1) == self.local_bell_index_1
        ns.sim_run()
        assert len(self.output_messages) == 2
        req = self.output_messages[0].request
        assert isinstance(req, ReqNewLinkUpdate)

    def test_register_new_link_nonlocal(self):
        self.tracker.register_new_link(link_identifier=self.nonlocal_link,
                                       bell_index=self.nonlocal_bell_index,
                                       times_of_birth={self.nodes[2].ID: 100,
                                                       self.nodes[3].ID: 320})
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.nonlocal_link)
        assert isinstance(entanglement_info, BellStateEntanglementInfo)
        assert entanglement_info.get_link_bell_index(self.nonlocal_link) == self.nonlocal_bell_index
        assert entanglement_info.get_time_of_birth(node_id=self.nodes[2].ID, link_identifier=self.nonlocal_link) == 100
        assert entanglement_info.get_time_of_birth(node_id=self.nodes[3].ID, link_identifier=self.nonlocal_link) == 320
        ns.sim_run()
        assert not self.output_messages

    def test_update_link_information_previously_untracked(self):
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=None,
                                       times_of_birth=None)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert entanglement_info.get_link_bell_index(self.local_link_1) is None
        assert entanglement_info.get_time_of_birth(node_id=self.node.ID, link_identifier=self.local_link_1) is None
        ns.sim_run()
        assert len(self.output_messages) == 2
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1,
                                       times_of_birth=None)
        assert entanglement_info.get_link_bell_index(self.local_link_1) == self.local_bell_index_1
        assert entanglement_info.get_time_of_birth(node_id=self.node.ID, link_identifier=self.local_link_1) is None
        ns.sim_run()
        assert len(self.output_messages) == 4
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=None,
                                       times_of_birth=100)
        assert entanglement_info.get_link_bell_index(self.local_link_1) == self.local_bell_index_1
        assert entanglement_info.get_time_of_birth(node_id=self.node.ID, link_identifier=self.local_link_1) == 100
        ns.sim_run()
        assert len(self.output_messages) == 6

    def test_update_link_information_already_tracked(self):
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1,
                                       times_of_birth=100)
        ns.sim_run()
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1,
                                       times_of_birth=100)
        ns.sim_run()
        assert len(self.output_messages) == 2  # no new info, so no new broadcast
        with self.assertRaises(RuntimeError):
            self.tracker.register_new_link(link_identifier=self.local_link_1,
                                           bell_index=self.local_bell_index_2,
                                           times_of_birth=100)
        with self.assertRaises(RuntimeError):
            self.tracker.register_new_link(link_identifier=self.local_link_1,
                                           bell_index=self.local_bell_index_1,
                                           times_of_birth=400)

    def register_swap(self, link_identifier_1, bell_index_1, link_identifier_2, bell_index_2):
        self.tracker.register_new_link(link_identifier=link_identifier_1,
                                       bell_index=bell_index_1,
                                       times_of_birth=10)
        self.tracker.register_new_link(link_identifier=link_identifier_2,
                                       bell_index=bell_index_2,
                                       times_of_birth=20)
        self.tracker.register_swap(link_identifier_1=link_identifier_1,
                                   link_identifier_2=link_identifier_2,
                                   bell_index=self.swap_bell_index,
                                   time_of_swap=100)
        [entanglement_info] = self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True)
        assert link_identifier_1 in entanglement_info.elementary_links
        assert entanglement_info.get_link_bell_index(link_identifier_1) == bell_index_1
        assert link_identifier_2 in entanglement_info.elementary_links
        assert entanglement_info.get_link_bell_index(link_identifier_2) == bell_index_2
        for node_id in link_identifier_1.node_ids & link_identifier_2.node_ids:
            assert entanglement_info.get_time_of_death(link_identifier=link_identifier_1, node_id=node_id) == 100
        assert entanglement_info.swap_bell_index(link_identifier_1, link_identifier_2) == self.swap_bell_index
        return entanglement_info

    def test_register_local_swap(self):
        self.message_filter = ReqSwapUpdate
        entanglement_info = self.register_swap(link_identifier_1=self.local_link_1,
                                               link_identifier_2=self.local_link_2,
                                               bell_index_1=self.local_bell_index_1,
                                               bell_index_2=self.local_bell_index_2)
        assert entanglement_info.bell_index == BellIndex.B11  # combination of B01, B10 and B00
        assert self.tracker.get_status(self.local_link_1) == LinkStatus.MEASURED
        assert self.tracker.get_status(self.local_link_2) == LinkStatus.MEASURED
        ns.sim_run()
        assert len(self.output_messages) == 2
        req = self.output_messages[0].request
        assert isinstance(req, ReqSwapUpdate)
        assert req.link_identifier_1 == self.local_link_1
        assert req.link_identifier_2 == self.local_link_2
        assert req.bell_index == self.swap_bell_index

    def test_register_nonlocal_swap(self):
        self.message_filter = ReqSwapUpdate
        entanglement_info = self.register_swap(link_identifier_1=self.local_link_2,
                                               link_identifier_2=self.nonlocal_link,
                                               bell_index_1=self.local_bell_index_2,
                                               bell_index_2=self.nonlocal_bell_index)
        assert entanglement_info.bell_index == BellIndex.B01  # combination of B10, B11 and B00
        ns.sim_run()
        assert not self.output_messages

    def test_register_nonlocal_swap_without_register_links(self):
        self.message_filter = ReqSwapUpdate
        self.tracker.register_swap(link_identifier_1=self.local_link_2,
                                   link_identifier_2=self.nonlocal_link,
                                   bell_index=self.swap_bell_index)
        [entanglement_info] = self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True)
        assert self.local_link_2 in entanglement_info.elementary_links
        assert self.nonlocal_link in entanglement_info.elementary_links
        assert entanglement_info.get_link_bell_index(self.local_link_2) is None
        assert entanglement_info.get_time_of_birth(link_identifier=self.local_link_2, node_id=self.node.ID) is None

    def test_register_local_discard(self):
        self.message_filter = ReqDiscardUpdate
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1)
        self.tracker.register_discard(link_identifier=self.local_link_1,
                                      node_id=self.node.ID,
                                      time_of_discard=101)
        assert self.tracker.get_status(self.local_link_1) == LinkStatus.DISCARDED
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert not entanglement_info.intact
        assert entanglement_info.get_time_of_death(link_identifier=self.local_link_1,
                                                   node_id=self.node.ID) == 101
        assert entanglement_info.get_time_of_death(link_identifier=self.local_link_1,
                                                   node_id=self.nodes[0].ID) is None
        assert self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True,
                                                                must_be_intact=False)
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True,
                                                                    must_be_intact=True)
        ns.sim_run()
        assert len(self.output_messages) == 2
        req = self.output_messages[0].request
        assert isinstance(req, ReqDiscardUpdate)
        assert req.link_identifier == self.local_link_1
        assert req.node_id == self.node.ID
        assert req.time_of_discard == 101

    def test_register_nonlocal_discard(self):
        self.message_filter = ReqDiscardUpdate
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1)
        self.tracker.register_discard(link_identifier=self.local_link_1,
                                      node_id=self.nodes[0].ID,
                                      time_of_discard=101)
        # Note: we expect the local qubit to also be discarded as a reaction to the remote discard
        assert self.tracker.get_status(self.local_link_1) == LinkStatus.DISCARDED
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert not entanglement_info.intact
        assert entanglement_info.get_time_of_death(link_identifier=self.local_link_1,
                                                   node_id=self.node.ID) == ns.sim_time()  # discarded right now
        assert entanglement_info.get_time_of_death(link_identifier=self.local_link_1,
                                                   node_id=self.nodes[0].ID) == 101
        assert self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True,
                                                                must_be_intact=False)
        ns.sim_run()
        assert len(self.output_messages) == 2
        req = self.output_messages[0].request
        assert isinstance(req, ReqDiscardUpdate)
        assert req.link_identifier == self.local_link_1
        assert req.node_id == self.node.ID
        assert req.time_of_discard == ns.sim_time()

    def test_register_nonlocal_discard_without_registered_link(self):
        self.tracker.register_discard(link_identifier=self.nonlocal_link,
                                      node_id=self.nodes[2].ID)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.nonlocal_link)
        assert entanglement_info is not None
        assert entanglement_info.elementary
        assert not entanglement_info.intact

    def test_register_local_measurement(self):
        self.message_filter = ReqMeasureUpdate
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1)
        self.tracker.register_measurement(link_identifier=self.local_link_1,
                                          time_of_measurement=654)
        assert self.tracker.get_status(self.local_link_1) == LinkStatus.MEASURED
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert entanglement_info.intact
        assert entanglement_info.get_time_of_death(node_id=self.node.ID, link_identifier=self.local_link_1) == 654
        assert len(entanglement_info.entangled_nodes) == 2
        assert len(self.output_messages) == 2
        req = self.output_messages[0].request
        assert isinstance(req, ReqMeasureUpdate)
        assert req.link_identifier == self.local_link_1
        assert req.node_id == self.node.ID
        assert req.time_of_measurement == 654

    def test_register_nonlocal_measurement(self):
        self.message_filter = ReqMeasureUpdate
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1)
        self.tracker.register_measurement(link_identifier=self.local_link_1,
                                          node_id=self.nodes[0].ID,
                                          time_of_measurement=654)
        assert self.tracker.get_status(self.local_link_1) == LinkStatus.AVAILABLE
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert entanglement_info.intact
        assert entanglement_info.get_time_of_death(node_id=self.nodes[0].ID, link_identifier=self.local_link_1) == 654
        assert len(entanglement_info.entangled_nodes) == 2
        assert not self.output_messages

    def test_register_nonlocal_measurement_without_registered_link(self):
        self.tracker.register_measurement(link_identifier=self.nonlocal_link,
                                          node_id=self.nodes[2].ID)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.nonlocal_link)
        assert entanglement_info is not None
        assert entanglement_info.elementary

    def test_get_entanglement_info(self):
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is None
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids={},
                                                                    other_nodes_allowed=True)
        self.tracker.register_new_link(link_identifier=self.local_link_1,
                                       bell_index=self.local_bell_index_1)
        self.tracker.register_new_link(link_identifier=self.local_link_2,
                                       bell_index=self.local_bell_index_2)
        self.tracker.register_new_link(link_identifier=self.nonlocal_link,
                                       bell_index=self.nonlocal_bell_index)
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1).elementary_links == {self.local_link_1}
        assert self.tracker.get_entanglement_info_of_link(self.local_link_2).elementary_links == {self.local_link_2}
        assert self.tracker.get_entanglement_info_of_link(self.nonlocal_link).elementary_links == {self.nonlocal_link}
        assert len(self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True)) == 3
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=False)
        assert (len(self.tracker.get_entanglement_info_between_nodes(node_ids={self.node.ID}, other_nodes_allowed=True))
                == 2)
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids={self.node.ID}, other_nodes_allowed=False)
        assert len(self.tracker.get_entanglement_info_between_nodes(node_ids={self.node.ID, self.nodes[0].ID},
                                                                    other_nodes_allowed=True)) == 1
        assert len(self.tracker.get_entanglement_info_between_nodes(node_ids={self.node.ID, self.nodes[0].ID},
                                                                    other_nodes_allowed=False)) == 1
        self.tracker.get_entanglement_info_of_link(self.local_link_1).intact = False
        assert len(self.tracker.get_entanglement_info_between_nodes(node_ids={self.node.ID},
                                                                    other_nodes_allowed=True,
                                                                    must_be_intact=True)) == 1
        assert len(self.tracker.get_entanglement_info_between_nodes(node_ids={self.node.ID},
                                                                    other_nodes_allowed=True,
                                                                    must_be_intact=False)) == 2

    class AwaitEntanglementTestProtocol(NodeProtocol):
        def __init__(self, node, tracker, node_ids, link_identifiers, other_nodes_allowed=False):
            super().__init__(node=node, name="await_entanglement_test_protocol")
            self.tracker = tracker
            self.node_ids = node_ids
            self.link_idetifiers = link_identifiers
            self.other_nodes_allowed = other_nodes_allowed
            self.finished = False

        def run(self):
            yield from self.tracker.await_entanglement(node_ids=self.node_ids,
                                                       link_identifiers=self.link_idetifiers,
                                                       other_nodes_allowed=self.other_nodes_allowed)
            self.finished = True

    def test_await_entanglement_single_link(self):
        await_entanglement_test_protocol = self.AwaitEntanglementTestProtocol(node=self.node,
                                                                              tracker=self.tracker,
                                                                              node_ids={self.node.ID,
                                                                                        self.nodes[0].ID},
                                                                              link_identifiers=set())
        await_entanglement_test_protocol.start()
        ns.sim_run()
        assert not await_entanglement_test_protocol.finished
        self.tracker.register_new_link(link_identifier=self.local_link_1, bell_index=self.local_bell_index_1)
        ns.sim_run()
        assert await_entanglement_test_protocol.finished

    def test_await_entanglement_with_swap(self):
        await_no_links = self.AwaitEntanglementTestProtocol(node=self.node,
                                                            tracker=self.tracker,
                                                            node_ids={self.node.ID,
                                                                      self.nodes[3].ID},
                                                            link_identifiers=set())
        await_with_link = self.AwaitEntanglementTestProtocol(node=self.node,
                                                             tracker=self.tracker,
                                                             node_ids={self.node.ID,
                                                                       self.nodes[3].ID},
                                                             link_identifiers={self.local_link_2})
        await_with_wrong_link = self.AwaitEntanglementTestProtocol(node=self.node,
                                                                   tracker=self.tracker,
                                                                   node_ids={self.node.ID,
                                                                             self.nodes[3].ID},
                                                                   link_identifiers={self.local_link_1})
        await_no_links.start()
        await_with_link.start()
        await_with_wrong_link.start()
        self.tracker.register_new_link(link_identifier=self.local_link_2,
                                       bell_index=self.local_bell_index_2)
        ns.sim_run()
        assert not await_no_links.finished
        assert not await_with_link.finished
        self.tracker.register_new_link(link_identifier=self.nonlocal_link,
                                       bell_index=self.nonlocal_bell_index)
        ns.sim_run()
        assert not await_no_links.finished
        assert not await_with_link.finished
        self.tracker.register_swap(link_identifier_1=self.nonlocal_link,
                                   link_identifier_2=self.local_link_2,
                                   bell_index=self.swap_bell_index)
        ns.sim_run()
        assert await_no_links.finished
        assert await_with_link.finished
        assert not await_with_wrong_link.finished

    def test_await_entanglement_with_discard(self):
        await_with_link = self.AwaitEntanglementTestProtocol(node=self.node,
                                                             tracker=self.tracker,
                                                             node_ids={self.node.ID,
                                                                       self.nodes[3].ID},
                                                             link_identifiers={self.local_link_2})
        await_with_link.start()
        self.tracker.register_new_link(link_identifier=self.local_link_2,
                                       bell_index=self.local_bell_index_2)
        ns.sim_run()
        assert not await_with_link.finished
        self.tracker.register_discard(link_identifier=self.local_link_2,
                                      node_id=self.nodes[2].ID)
        ns.sim_run()
        # should be finished because target state cannot be reached anymore now that link was discarded
        assert await_with_link.finished

    def test_await_entanglement_with_swap_no_link_identifier(self):
        await_entanglement_test_protocol = self.AwaitEntanglementTestProtocol(node=self.node,
                                                                              tracker=self.tracker,
                                                                              node_ids={self.node.ID,
                                                                                        self.nodes[3].ID},
                                                                              link_identifiers=set())
        await_entanglement_test_protocol.start()
        self.tracker.register_new_link(link_identifier=self.local_link_2,
                                       bell_index=self.local_bell_index_2)
        ns.sim_run()
        assert not await_entanglement_test_protocol.finished
        self.tracker.register_new_link(link_identifier=self.nonlocal_link,
                                       bell_index=self.nonlocal_bell_index)
        ns.sim_run()
        assert not await_entanglement_test_protocol.finished
        self.tracker.register_swap(link_identifier_1=self.nonlocal_link,
                                   link_identifier_2=self.local_link_2,
                                   bell_index=self.swap_bell_index)
        ns.sim_run()
        assert await_entanglement_test_protocol.finished


class TestBellStateTrackerWithUntracking(TestBellStateTrackerWithoutUntracking):
    """Test `BellStateTracker` when set to automatically untrack stale entanglement.

    All tests from `TestBellStateTrackerWithoutUntracking` are repeated but with
    `BellStateTracker.untrack_stale_entanglement = True`.
    There are also additional tests to check if entanglement is correctly automatically untracked,
    and tests to see if `BellStateTracker.untrack_entanglement_info` works as expected.

    """

    def set_up_tracker(self):
        self.tracker = BellStateTracker(node=self.node, name="test_bell_state_tracker",
                                        untrack_stale_entanglement=True)

    def test_untrack_stale_entanglement_property(self):
        assert self.tracker.untrack_stale_entanglement is True

    def test_entanglement_is_stale_local(self):
        """Local entanglement can never go stale."""
        self.tracker.register_new_link(link_identifier=self.local_link_1)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert not self.tracker._entanglement_is_stale(entanglement_info)
        for node_id in self.local_link_1.node_ids:
            entanglement_info.set_time_of_death(node_id=node_id,
                                                link_identifier=self.local_link_1,
                                                time_of_death=0)
        assert not entanglement_info.has_alive_qubits
        assert not self.tracker._entanglement_is_stale(entanglement_info)

    def test_entanglement_is_stale_nonlocal(self):
        """Nonlocal entanglement is stale when there are no alive qubits left."""
        self.tracker.register_new_link(link_identifier=self.nonlocal_link)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.nonlocal_link)
        assert not self.tracker._entanglement_is_stale(entanglement_info)
        for node_id in self.nonlocal_link.node_ids:
            entanglement_info.set_time_of_death(node_id=node_id,
                                                link_identifier=self.nonlocal_link,
                                                time_of_death=0)
        assert not entanglement_info.has_alive_qubits
        assert self.tracker._entanglement_is_stale(entanglement_info)

    def test_untrack_stale_entanglement_discard(self):
        """Check entanglement is automatically untracked if it becomes stale by discard."""
        self.tracker.register_new_link(link_identifier=self.nonlocal_link)
        assert self.tracker.get_entanglement_info_of_link(self.nonlocal_link) is not None
        for node_id in self.nonlocal_link.node_ids:
            self.tracker.register_discard(link_identifier=self.nonlocal_link,
                                          node_id=node_id)
        assert self.tracker.get_entanglement_info_of_link(self.nonlocal_link) is None

    def test_untrack_stale_entanglement_measurement(self):
        """Check entanglement is automatically untracked if it becomes stale by measurement."""
        self.tracker.register_new_link(link_identifier=self.nonlocal_link)
        assert self.tracker.get_entanglement_info_of_link(self.nonlocal_link) is not None
        for node_id in self.nonlocal_link.node_ids:
            self.tracker.register_measurement(link_identifier=self.nonlocal_link,
                                              node_id=node_id)
        assert self.tracker.get_entanglement_info_of_link(self.nonlocal_link) is None

    def test_untrack_stale_entanglement_swap(self):
        """Check entanglement is automatically untracked if it becomes stale by swap."""
        self.tracker.register_new_link(link_identifier=self.local_link_1)
        self.tracker.register_new_link(link_identifier=self.local_link_2)
        self.tracker.register_measurement(link_identifier=self.local_link_1,
                                          node_id=0)
        self.tracker.register_measurement(link_identifier=self.local_link_2,
                                          node_id=2)
        assert len(self.tracker.get_entanglement_info_between_nodes(node_ids=set(), other_nodes_allowed=True)) == 2
        self.tracker.register_swap(link_identifier_1=self.local_link_1, link_identifier_2=self.local_link_2)
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is None
        assert self.tracker.get_entanglement_info_of_link(self.local_link_2) is None
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids=set(), other_nodes_allowed=True)

    def test_untrack_entanglement_without_alive_qubits(self):
        """Check that manually untracking entanglement without alive qubits works."""
        self.tracker.register_new_link(link_identifier=self.local_link_1)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert (self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)
                == [entanglement_info])
        assert entanglement_info is not None
        for node_id in self.local_link_1.node_ids:
            self.tracker.register_measurement(link_identifier=self.local_link_1,
                                              node_id=node_id)
        assert not entanglement_info.has_alive_qubits
        self.tracker.untrack_entanglement_info(entanglement_info)
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is None
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)

    def test_untrack_entanglement_with_alive_qubits(self):
        """Check that untracking entanglement with alive qubits has no effect."""
        self.tracker.register_new_link(link_identifier=self.local_link_1)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert (self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)
                == [entanglement_info])
        assert entanglement_info.has_alive_qubits
        self.tracker.untrack_entanglement_info(entanglement_info)
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is entanglement_info
        assert (self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)
                == [entanglement_info])
        # also check if entanglement_info is removed of the to-untrack-list after swap
        assert entanglement_info in self.tracker._entanglement_info_to_be_untracked
        self.tracker.register_new_link(link_identifier=self.local_link_2)
        self.tracker.register_swap(link_identifier_1=self.local_link_1, link_identifier_2=self.local_link_2)
        assert not self.tracker._entanglement_info_to_be_untracked

    def test_untrack_entanglement_with_delayed_qubit_measurement(self):
        """Check that if entanglement with alive qubits is untracked, it only takes effect after the qubits die."""
        self.tracker.register_new_link(link_identifier=self.local_link_1)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert (self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)
                == [entanglement_info])
        assert entanglement_info.has_alive_qubits
        self.tracker.untrack_entanglement_info(entanglement_info)  # expect this to have no effect
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is entanglement_info
        assert (self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)
                == [entanglement_info])
        for node_id in self.local_link_1.node_ids:
            self.tracker.register_measurement(link_identifier=self.local_link_1,
                                              node_id=node_id)
        # now, there should be no alive qubits left, and the untracking should take place
        assert not entanglement_info.has_alive_qubits
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is None
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)

    def test_untrack_entanglement_with_delayed_qubit_discard(self):
        """Check that if entanglement with alive qubits is untracked, it only takes effect after the qubits die."""
        self.tracker.register_new_link(link_identifier=self.local_link_1)
        entanglement_info = self.tracker.get_entanglement_info_of_link(self.local_link_1)
        assert (self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)
                == [entanglement_info])
        assert entanglement_info.has_alive_qubits
        self.tracker.untrack_entanglement_info(entanglement_info)  # expect this to have no effect
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is entanglement_info
        assert (self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)
                == [entanglement_info])
        self.tracker.register_discard(link_identifier=self.local_link_1,
                                      node_id=0)  # Note: this discards remote qubit, but also the local one in response
        # now, there should be no alive qubits left, and the untracking should take place
        assert not entanglement_info.has_alive_qubits
        assert self.tracker.get_entanglement_info_of_link(self.local_link_1) is None
        assert not self.tracker.get_entanglement_info_between_nodes(node_ids=self.local_link_1.node_ids)


def _calculate_line_forwarding_table(nodes: List[Node]):
    forwarding_table = {}
    for i, node in enumerate(nodes):

        local_routing_table = {}
        for j, remote_node in enumerate(nodes):
            if node is not remote_node:
                if j < i:
                    port = node.ports["A"]
                else:
                    port = node.ports["B"]

                local_routing_table[remote_node.name] = port
            forwarding_table[node.name] = local_routing_table
    return forwarding_table


def test_bell_state_tracking_in_network():
    """Three-node linear network. Nodes 0, 1 and 2 are connected by classical connections to test broadcasting updates.

    1. register entanglement between nodes 0 and 1.
    2. register entanglement between nodes 1 and 2
    3. register swap at node 1
    4. register discard at node 2
    5. register entanglement between 0 and 1 and register measure at 1

    """
    # set up the network
    nodes = [Node(f"node_{i}", ID=i, port_names=["A", "B"]) for i in range(3)]
    forwarding_table = _calculate_line_forwarding_table(nodes
                                                        )
    for node in nodes:
        node.driver = Driver(f"driver_of_{node.name}")
        node.driver.add_service(EntanglementTrackerService, BellStateTracker(node=node, name=f"tracker_of_{node.name}",
                                                                             untrack_stale_entanglement=False))
        routing_service = ClassicalRoutingService(node=node, local_routing_table=forwarding_table[node.name])
        node.driver.add_service(ClassicalRoutingService,
                                routing_service)
        node.add_subcomponent(node.driver)

    for i in range(2):
        nodes[i].connect_to(remote_node=nodes[i + 1],
                            connection=ClassicalConnection(f"connection_{nodes[i].name}_{nodes[i + 1].name}"),
                            local_port_name="B", remote_port_name="A")
    trackers = [node.driver[EntanglementTrackerService] for node in nodes]
    for node in nodes:
        node.driver[ClassicalRoutingService].start()

    # set up the parameters
    links = [LinkIdentifier(node_ids=frozenset({nodes[i].ID, nodes[i + 1].ID}), pair_identifier=100) for i in range(2)]
    link_bell_indices = [BellIndex.B01, BellIndex.B10]
    swap_bell_index = BellIndex.B11
    link_times = [14, 56]
    time_of_swap = 112

    # set up protocol that tests await_entanglement
    class AwaitEntanglementProtocol(NodeProtocol):
        def __init__(self):
            super().__init__(node=nodes[0], name="await_entanglement_test_protocol")
            self.finished = False

        def run(self):
            yield from trackers[0].await_entanglement(node_ids={nodes[0].ID, nodes[2].ID})
            self.finished = True
    await_entanglement_protocol = AwaitEntanglementProtocol()
    await_entanglement_protocol.start()

    # 1. entanglement 0 - 1
    trackers[1].register_new_link(link_identifier=links[0], bell_index=link_bell_indices[0],
                                  times_of_birth={nodes[0].ID: None, nodes[1].ID: link_times[0]})
    ns.sim_run()
    assert not await_entanglement_protocol.finished
    assert trackers[1].num_available_links == 1
    assert trackers[0].num_available_links == 1
    for tracker in trackers:
        ent_info = tracker.get_entanglement_info_of_link(links[0])
        assert ent_info.entangled_nodes == {nodes[0].ID, nodes[1].ID}
        assert ent_info.get_time_of_birth(node_id=nodes[1].ID, link_identifier=links[0]) == link_times[0]
        assert ent_info.get_time_of_birth(node_id=nodes[0].ID, link_identifier=links[0]) is None

    # now register link again but not with time of birth info at node 0 (each node only registering own birth date)
    trackers[0].register_new_link(link_identifier=links[0], bell_index=link_bell_indices[0],
                                  times_of_birth={nodes[0].ID: link_times[0], nodes[1].ID: None})
    ns.sim_run()
    for tracker in trackers:
        ent_info = tracker.get_entanglement_info_of_link(links[0])
        assert ent_info.entangled_nodes == {nodes[0].ID, nodes[1].ID}
        assert ent_info.get_time_of_birth(node_id=nodes[1].ID, link_identifier=links[0]) == link_times[0]
        assert ent_info.get_time_of_birth(node_id=nodes[0].ID, link_identifier=links[0]) == link_times[0]

    # 2. entanglement 1 - 2 (this time setting both times of birth in one registration)
    trackers[1].register_new_link(link_identifier=links[1], bell_index=link_bell_indices[1],
                                  times_of_birth=link_times[1])
    ns.sim_run()
    assert not await_entanglement_protocol.finished
    for tracker in trackers:
        ent_info = tracker.get_entanglement_info_of_link(links[1])
        assert ent_info.entangled_nodes == {nodes[1].ID, nodes[2].ID}
        assert ent_info.get_time_of_birth(node_id=nodes[1].ID, link_identifier=links[1]) == link_times[1]
        assert ent_info.get_time_of_birth(node_id=nodes[2].ID, link_identifier=links[1]) == link_times[1]

    # 3. swap at 1
    trackers[1].register_swap(link_identifier_1=links[0], link_identifier_2=links[1],
                              bell_index=swap_bell_index, time_of_swap=time_of_swap)
    ns.sim_run()
    assert await_entanglement_protocol.finished
    for tracker in trackers:
        assert not tracker.get_entanglement_info_between_nodes(node_ids={nodes[1].ID}, other_nodes_allowed=True)
        [ent_info] = tracker.get_entanglement_info_between_nodes(node_ids={nodes[0].ID, nodes[2].ID},
                                                                 other_nodes_allowed=False)
        assert len(ent_info.elementary_links) == 2
        assert ent_info.bell_index == BellIndex.B00  # combining B01, B10 and B11
        for link in links:
            assert ent_info.get_time_of_death(node_id=nodes[1].ID, link_identifier=link) == time_of_swap

    # 4. discard at 2 (which should also lead to discard at 1 as response
    trackers[2].register_discard(link_identifier=links[1])
    ns.sim_run()
    assert trackers[0].get_status(links[0]) == LinkStatus.DISCARDED
    assert trackers[2].get_status(links[1]) == LinkStatus.DISCARDED
    for tracker in trackers:
        assert not tracker.get_entanglement_info_between_nodes(node_ids={},
                                                               other_nodes_allowed=True,
                                                               must_be_intact=True)
        [ent_info] = tracker.get_entanglement_info_between_nodes(node_ids={nodes[0].ID, nodes[2].ID},
                                                                 other_nodes_allowed=False,
                                                                 must_be_intact=False)
        assert not ent_info.intact
        assert ent_info.get_time_of_death(node_id=nodes[0].ID, link_identifier=links[0]) is not None
        assert ent_info.get_time_of_death(node_id=nodes[2].ID, link_identifier=links[1]) is not None

    # 5. new entanglement 0 - 1 and measure at 1
    link = LinkIdentifier(node_ids=frozenset([nodes[0].ID, nodes[1].ID]), pair_identifier=324)
    trackers[0].register_new_link(link_identifier=link, bell_index=BellIndex.B00)
    ns.sim_run()
    trackers[1].register_measurement(link_identifier=link, node_id=None)
    ns.sim_run()
    for tracker in trackers:
        [ent_info] = tracker.get_entanglement_info_between_nodes(node_ids={}, other_nodes_allowed=True,
                                                                 must_be_intact=True)
        assert ent_info.get_time_of_death(node_id=nodes[1].ID, link_identifier=link) is not None
        assert ent_info.get_time_of_death(node_id=nodes[0].ID, link_identifier=link) is None
