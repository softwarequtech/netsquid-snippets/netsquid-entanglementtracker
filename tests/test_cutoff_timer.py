import netsquid as ns
import unittest

from netsquid.qubits.ketstates import BellIndex
from netsquid.nodes import Node

from netsquid_driver.driver import Driver
from netsquid_entanglementtracker.local_link_tracker import LocalLinkTracker
from netsquid_entanglementtracker.cutoff_timer import CutoffTimer, LinkCutoffTimer
from netsquid_driver.entanglement_tracker_service import LinkIdentifier, LinkStatus, \
    EntanglementTrackerService


class TestCutoffTimer(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        node_id = 0
        self.node = Node("test_cutoff_timer_node", ID=node_id)
        self.link1 = LinkIdentifier(node_ids=frozenset({node_id, node_id + 1}), pair_identifier=0)
        self.link2 = LinkIdentifier(node_ids=frozenset({node_id, node_id + 2}), pair_identifier=1)

        # set up driver
        self.driver = Driver(name="test_driver")
        self.node.add_subcomponent(self.driver)
        self.node.driver = self.driver

        # set up LocalLinkTracker
        self.tracker = LocalLinkTracker(node=self.node, name="test_local_link_tracker")
        self.node.driver.add_service(EntanglementTrackerService, self.tracker)

    def check_cutoff_works(self, cutoff, links):
        """Check that link is available before the timer runs out and discarded after the timer runs out."""
        ns.sim_run(duration=cutoff._cutoff_time / 10)
        for link in links:
            assert self.tracker.get_status(link) == LinkStatus.AVAILABLE

        ns.sim_run(duration=cutoff._cutoff_time * 2)
        for link in links:
            assert self.tracker.get_status(link) == LinkStatus.DISCARDED

    def test_cutoff_for_subsequent_links(self):
        """Cutoff is implemented for two links created at different times."""
        cutoff = CutoffTimer(node=self.node, cutoff_time=50, name="test_cutoff_timer")
        cutoff.start()

        self.tracker.register_new_link(link_identifier=self.link1, bell_index=BellIndex.B01)
        self.check_cutoff_works(cutoff=cutoff, links=[self.link1])

        self.tracker.register_new_link(link_identifier=self.link2, bell_index=BellIndex.B01)
        self.check_cutoff_works(cutoff=cutoff, links=[self.link2])

    def test_cutoff_for_parallel_links(self):
        """Cutoff is implemented for two links registered right after each other."""
        cutoff = CutoffTimer(node=self.node, cutoff_time=50, name="test_cutoff_timer")
        cutoff.start()

        self.tracker.register_new_link(link_identifier=self.link1, bell_index=BellIndex.B01)
        self.tracker.register_new_link(link_identifier=self.link2, bell_index=BellIndex.B01)
        self.check_cutoff_works(cutoff=cutoff, links=[self.link1, self.link2])


class TestLinkCutoffTimer(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        node_id = 0
        node = Node("test_cutoff_service_node", ID=node_id)
        self.link1 = LinkIdentifier(node_ids=frozenset({node_id, node_id + 1}), pair_identifier=0)
        self.link2 = LinkIdentifier(node_ids=frozenset({node_id, node_id + 2}), pair_identifier=1)

        # set up driver
        self.driver = Driver(name="test_driver")
        node.add_subcomponent(self.driver)
        node.driver = self.driver

        # set up LocalLinkTracker and register link1 so it can be later discarded
        self.tracker = LocalLinkTracker(node=node, name="test_local_link_tracker")
        node.driver.add_service(EntanglementTrackerService, self.tracker)
        self.tracker.register_new_link(link_identifier=self.link1, bell_index=BellIndex.B01)

        # set up cutoff
        self.cutoff_time = 50
        cutoff = CutoffTimer(node=node, cutoff_time=self.cutoff_time, name="test_cutoff_service")
        self.timer = LinkCutoffTimer(node=node, superprotocol=cutoff, cutoff_time=self.cutoff_time,
                                     link_identifier=self.link1)

    def test_link_gets_discarded(self):
        """Protocol stops running after a link is discarded."""
        self.timer.start()

        ns.sim_run(duration=self.cutoff_time * 2)

        assert self.tracker.get_status(self.link1) == LinkStatus.DISCARDED
        assert not self.timer.is_running

    def test_link_is_measured(self):
        """When the link is measured out before the timer runs out, protocol simply stops."""
        self.timer.start()

        ns.sim_run(duration=self.cutoff_time / 2)

        assert self.tracker.get_status(self.link1) == LinkStatus.AVAILABLE
        self.tracker.register_measurement(self.link1)
        assert self.tracker.get_status(self.link1) == LinkStatus.MEASURED

        ns.sim_run(duration=self.cutoff_time * 2)

        assert self.tracker.get_status(self.link1) == LinkStatus.MEASURED
        assert not self.timer.is_running
