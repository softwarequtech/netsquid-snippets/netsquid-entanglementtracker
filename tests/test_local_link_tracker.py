import netsquid as ns
import unittest
import logging
from netsquid.nodes.node import Node
from netsquid.qubits.ketstates import BellIndex
from netsquid.protocols.protocol import Protocol
from netsquid_entanglementtracker.local_link_tracker import LocalLinkTracker, LinkStatus
from netsquid_driver.entanglement_tracker_service import LinkIdentifier, ReqNewLinkUpdate, \
    ReqDiscardUpdate, ReqSwapUpdate, ResNewLocalLink, ResLocalDiscard, ResLocalMeasure, ReqMeasureUpdate


class CheckResponsesOfEntanglementTrackerProtocol(Protocol):

    def __init__(self, entanglement_tracker):
        super().__init__(name="check_responses_of_entanglement_tracker_protocol")
        self.entanglement_tracker = entanglement_tracker
        self.has_seen_res_new_link = 0
        self.has_seen_res_discard = 0
        self. has_seen_res_measure = 0

    def run(self):
        while True:
            evt1 = self.await_signal(sender=self.entanglement_tracker,
                                     signal_label=ResNewLocalLink.__name__)
            evt2 = self.await_signal(sender=self.entanglement_tracker,
                                     signal_label=ResLocalDiscard.__name__)
            evt3 = self.await_signal(sender=self.entanglement_tracker,
                                     signal_label=ResLocalMeasure.__name__)
            evt_expr = yield evt1 | (evt2 | evt3)
            [evt] = evt_expr.triggered_events
            res = self.entanglement_tracker.get_signal_by_event(evt).result
            if evt_expr.first_term.value:
                self.has_seen_res_new_link += 1
                assert isinstance(res, ResNewLocalLink)
            elif evt_expr.second_term.first_term.value:
                self.has_seen_res_discard += 1
                assert isinstance(res, ResLocalDiscard)
            elif evt_expr.second_term.second_term.value:
                self.has_seen_res_measure += 1
                assert isinstance(res, ResLocalMeasure)
            else:
                assert False  # this should never be reached


class TestLocalLinkTracker(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ns.logger.setLevel(logging.DEBUG)
        node_id = 0
        cls.node = Node("test_local_link_tracker_node", ID=node_id)
        cls.link1 = LinkIdentifier(node_ids=frozenset({node_id, node_id + 1}), pair_identifier=0)
        cls.link2 = LinkIdentifier(node_ids=frozenset({node_id, node_id + 2}), pair_identifier=3)
        cls.nonlocal_link = LinkIdentifier(node_ids=frozenset({node_id + 1, node_id + 2}), pair_identifier=0)
        cls.bell_state = BellIndex.B01
        cls.mem_pos_1 = 0
        cls.mem_pos_2 = 1
        cls.time_of_birth = 0
        cls.duration = 100

    def setUp(self):
        ns.sim_reset()
        self.tracker = LocalLinkTracker(node=self.node, name="test_local_link_tracker")
        self.checker = CheckResponsesOfEntanglementTrackerProtocol(self.tracker)
        self.checker.start()

    def register_link1(self):
        self.tracker.register_new_link(link_identifier=self.link1,
                                       bell_index=self.bell_state,
                                       mem_pos=self.mem_pos_1,
                                       times_of_birth=self.time_of_birth)

    def register_link2(self):
        self.tracker.register_new_link(link_identifier=self.link2,
                                       bell_index=self.bell_state,
                                       mem_pos=self.mem_pos_2,
                                       times_of_birth=self.time_of_birth)

    def assert_new_link_registered(self):
        assert self.tracker.num_available_links == 1
        assert self.tracker.get_status(self.link1) == LinkStatus.AVAILABLE
        assert self.tracker.get_links_by_status(LinkStatus.AVAILABLE) == [self.link1]
        assert not self.tracker.get_links_by_status(LinkStatus.MEASURED)
        assert not self.tracker.get_links_by_status(LinkStatus.DISCARDED)
        assert self.tracker.get_bell_index(self.link1) == self.bell_state
        ns.sim_run()
        assert self.checker.has_seen_res_new_link == 1

    def assert_discard_registered(self):
        assert self.tracker.num_available_links == 0
        assert self.tracker.get_status(self.link1) == LinkStatus.DISCARDED
        assert not self.tracker.get_links_by_status(LinkStatus.AVAILABLE)
        assert not self.tracker.get_links_by_status(LinkStatus.MEASURED)
        assert self.tracker.get_links_by_status(LinkStatus.DISCARDED) == [self.link1]
        ns.sim_run(duration=10 * self.duration)  # to check that the lifetime does not increase after discard
        assert self.tracker.get_local_link_lifetime(self.link1) == self.duration
        assert self.tracker.get_mem_pos(self.link1) is None
        assert self.checker.has_seen_res_discard == 1

    def assert_measurement_registered(self):
        assert self.tracker.num_available_links == 0
        assert self.tracker.get_status(self.link1) == LinkStatus.MEASURED
        assert not self.tracker.get_links_by_status(LinkStatus.AVAILABLE)
        assert not self.tracker.get_links_by_status(LinkStatus.DISCARDED)
        assert self.tracker.get_links_by_status(LinkStatus.MEASURED) == [self.link1]
        ns.sim_run(duration=10 * self.duration)  # to check that the lifetime does not increase after discard
        assert self.tracker.get_local_link_lifetime(self.link1) == self.duration
        assert self.tracker.get_mem_pos(self.link1) is None
        assert self.checker.has_seen_res_measure == 1

    def assert_swap_registered(self):
        assert self.tracker.num_available_links == 0
        assert self.tracker.get_status(self.link1) == LinkStatus.MEASURED
        assert self.tracker.get_status(self.link2) == LinkStatus.MEASURED
        assert not self.tracker.get_links_by_status(LinkStatus.AVAILABLE)
        assert not self.tracker.get_links_by_status(LinkStatus.DISCARDED)
        assert self.tracker.get_links_by_status(LinkStatus.MEASURED) == [self.link1, self.link2]
        assert self.tracker.get_local_link_lifetime(self.link1) == self.duration
        assert self.tracker.get_mem_pos(self.link1) is None
        assert self.tracker.get_mem_pos(self.link2) is None
        ns.sim_run()
        assert self.checker.has_seen_res_measure == 2

    def test_no_registration(self):

        assert self.tracker.num_available_links == 0
        assert not self.tracker.get_links_by_status(LinkStatus.AVAILABLE)
        assert not self.tracker.get_links_by_status(LinkStatus.MEASURED)
        assert not self.tracker.get_links_by_status(LinkStatus.DISCARDED)
        assert self.tracker.get_mem_pos(self.link1) is None
        assert self.tracker.get_link(self.mem_pos_1) is None
        assert self.checker.has_seen_res_new_link == 0
        assert self.checker.has_seen_res_discard == 0
        assert self.checker.has_seen_res_measure == 0

    def test_register_new_link(self):

        with self.assertRaises(ValueError):
            self.tracker.register_new_link(link_identifier=self.nonlocal_link,
                                           bell_index=self.bell_state)

        self.register_link1()
        self.assert_new_link_registered()
        assert self.tracker.get_mem_pos(self.link1) == self.mem_pos_1
        assert self.tracker.get_link(self.mem_pos_1) == self.link1

    def test_register_new_link_twice(self):
        # second update contains no new information and should thus not change anything
        self.register_link1()
        self.register_link1()
        self.assert_new_link_registered()
        assert self.tracker.get_mem_pos(self.link1) == self.mem_pos_1
        assert self.tracker.get_local_link_lifetime(self.link1) == ns.sim_time() - self.time_of_birth

    def test_register_new_local_link(self):
        self.register_link1()
        self.tracker.register_new_local_link(remote_node_id=self.node.ID + 1,  # same as link1
                                             bell_index=self.bell_state)
        available_links = self.tracker.get_links_by_status(LinkStatus.AVAILABLE)
        assert len(available_links) == 2
        assert self.link1 in available_links
        [other_link] = [link for link in available_links if link != self.link1]
        assert other_link.pair_identifier == self.link1.pair_identifier + 1

    def test_register_new_link_by_request(self):

        with self.assertRaises(ValueError):
            req = ReqNewLinkUpdate(link_identifier=self.nonlocal_link,
                                   bell_index=self.bell_state)
            self.tracker.put(req)

        req = ReqNewLinkUpdate(link_identifier=self.link1,
                               bell_index=self.bell_state,
                               times_of_birth=self.time_of_birth)
        self.tracker.put(req)
        self.assert_new_link_registered()

    def test_register_new_link_unequal_times_of_birth(self):

        times_of_birth = {self.node.ID: self.time_of_birth,
                          self.node.ID + 1: 10 * self.time_of_birth}
        self.tracker.register_new_link(link_identifier=self.link1,
                                       bell_index=self.bell_state,
                                       mem_pos=self.mem_pos_1,
                                       times_of_birth=times_of_birth)
        self.assert_new_link_registered()
        assert self.tracker._local_links[self.link1].time_of_birth == self.time_of_birth

    def test_register_no_tracking(self):
        self.tracker.register_new_link(link_identifier=self.link1,
                                       bell_index=None,
                                       mem_pos=None,
                                       times_of_birth=None)
        assert self.tracker.get_mem_pos(self.link1) is None
        assert self.tracker.get_bell_index(self.link1) is None
        assert self.tracker.get_local_link_lifetime(self.link1) is None

    def test_update_mem_pos_not_yet_tracked(self):
        self.tracker.register_new_link(link_identifier=self.link1,
                                       bell_index=self.bell_state,
                                       mem_pos=None,
                                       times_of_birth=self.time_of_birth)
        self.register_link1()
        self.assert_new_link_registered()
        assert self.tracker.get_mem_pos(self.link1) == self.mem_pos_1

    def test_update_mem_pos_already_tracked(self):
        self.register_link1()
        with self.assertRaises(RuntimeError):
            self.tracker.register_new_link(link_identifier=self.link1,
                                           bell_index=self.bell_state,
                                           mem_pos=self.mem_pos_1 + 1,
                                           times_of_birth=self.time_of_birth)

    def test_update_time_of_birth_not_yet_tracked(self):
        self.tracker.register_new_link(link_identifier=self.link1,
                                       bell_index=self.bell_state,
                                       mem_pos=self.mem_pos_1,
                                       times_of_birth=None)
        self.register_link1()
        self.assert_new_link_registered()
        assert self.tracker._local_links[self.link1].time_of_birth == self.time_of_birth

    def test_update_time_of_birth_already_tracked(self):
        self.register_link1()
        with self.assertRaises(RuntimeError):
            self.tracker.register_new_link(link_identifier=self.link1,
                                           bell_index=self.bell_state,
                                           mem_pos=self.mem_pos_1,
                                           times_of_birth=self.time_of_birth + 100)

    def test_update_bell_index_not_yet_tracked(self):
        self.tracker.register_new_link(link_identifier=self.link1,
                                       bell_index=None,
                                       mem_pos=self.mem_pos_1,
                                       times_of_birth=self.time_of_birth)
        self.register_link1()
        self.assert_new_link_registered()
        assert self.tracker.get_bell_index(self.link1) == self.bell_state

    def test_update_bell_index_already_tracked(self):
        self.register_link1()
        with self.assertRaises(RuntimeError):
            self.tracker.register_new_link(link_identifier=self.link1,
                                           bell_index=BellIndex(0),
                                           mem_pos=self.mem_pos_1,
                                           times_of_birth=self.time_of_birth + 100)

    def test_register_move(self):
        self.register_link1()
        self.tracker.register_move(old_mem_pos=self.mem_pos_1, new_mem_pos=self.mem_pos_2)
        assert self.tracker.get_mem_pos(self.link1) == self.mem_pos_2
        assert self.tracker.get_link(self.mem_pos_1) is None
        assert self.tracker.get_link(self.mem_pos_2) == self.link1

    def test_register_discard(self):

        with self.assertRaises(ValueError):
            self.tracker.register_discard(link_identifier=self.link1)

        self.register_link1()
        ns.sim_run(duration=self.duration)
        self.tracker.register_discard(link_identifier=self.link1,
                                      time_of_discard=None)
        self.assert_discard_registered()

    def test_register_discard_by_mem_pos(self):

        self.register_link1()
        ns.sim_run(duration=self.duration)
        self.tracker.register_local_discard_mem_pos(mem_pos=self.mem_pos_1,
                                                    time_of_discard=ns.sim_time())
        self.assert_discard_registered()

    def test_register_discard_by_request(self):
        self.register_link1()
        ns.sim_run(duration=self.duration)
        with self.assertRaises(ValueError):
            req = ReqDiscardUpdate(link_identifier=self.link1,
                                   node_id=self.node.ID + 1,
                                   time_of_discard=None)
            self.tracker.put(req)
        with self.assertRaises(ValueError):
            req = ReqDiscardUpdate(link_identifier=self.nonlocal_link,
                                   node_id=self.node.ID)
            self.tracker.put(req)
        req = ReqDiscardUpdate(link_identifier=self.link1,
                               node_id=self.node.ID,
                               time_of_discard=None)
        self.tracker.put(req)
        self.assert_discard_registered()

    def test_register_measurement(self):

        with self.assertRaises(ValueError):
            self.tracker.register_measurement(link_identifier=self.link1)

        self.register_link1()
        ns.sim_run(duration=self.duration)
        self.tracker.register_measurement(link_identifier=self.link1,
                                          time_of_measurement=None)
        self.assert_measurement_registered()

    def test_register_measurement_by_request(self):
        self.register_link1()
        ns.sim_run(duration=self.duration)
        with self.assertRaises(ValueError):
            req = ReqMeasureUpdate(link_identifier=self.link1,
                                   node_id=self.node.ID + 1,
                                   time_of_measurement=None)
            self.tracker.put(req)
        with self.assertRaises(ValueError):
            req = ReqMeasureUpdate(link_identifier=self.nonlocal_link,
                                   node_id=self.node.ID)
            self.tracker.put(req)
        req = ReqMeasureUpdate(link_identifier=self.link1,
                               node_id=self.node.ID,
                               time_of_measurement=None)
        self.tracker.put(req)
        self.assert_measurement_registered()

    def test_register_measurement_by_mem_pos(self):

        self.register_link1()
        ns.sim_run(duration=self.duration)
        self.tracker.register_local_measurement_mem_pos(mem_pos=self.mem_pos_1,
                                                        time_of_measurement=ns.sim_time())
        self.assert_measurement_registered()

    def test_register_swap(self):
        self.register_link1()
        self.register_link2()
        assert self.tracker.num_available_links == 2
        ns.sim_run(duration=self.duration)
        self.tracker.register_swap(link_identifier_1=self.link1,
                                   link_identifier_2=self.link2,
                                   bell_index=self.bell_state,
                                   time_of_swap=None)
        self.assert_swap_registered()

    def test_register_swap_by_mem_pos(self):
        # this should do nothing
        self.tracker.register_local_swap_mem_pos(mem_pos_1=self.mem_pos_1,
                                                 mem_pos_2=self.mem_pos_2,
                                                 bell_index=self.bell_state)
        self.register_link1()
        with self.assertRaises(ValueError):
            self.tracker.register_local_swap_mem_pos(mem_pos_1=self.mem_pos_1,
                                                     mem_pos_2=self.mem_pos_2,
                                                     bell_index=self.bell_state)
        self.register_link2()
        ns.sim_run(duration=self.duration)
        self.tracker.register_local_swap_mem_pos(mem_pos_1=self.mem_pos_1,
                                                 mem_pos_2=self.mem_pos_2,
                                                 bell_index=self.bell_state,
                                                 time_of_swap=ns.sim_time())

    def test_register_swap_by_request(self):
        self.register_link1()
        self.register_link2()
        ns.sim_run(duration=self.duration)
        req = ReqSwapUpdate(link_identifier_1=self.link1,
                            link_identifier_2=self.link2,
                            bell_index=self.bell_state)
        self.tracker.put(req)
        self.assert_swap_registered()

    def test_lifetime(self):
        self.register_link1()
        ns.sim_run(duration=self.duration)
        assert self.tracker.get_local_link_lifetime(self.link1) == self.duration
