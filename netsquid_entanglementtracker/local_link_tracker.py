from dataclasses import dataclass
from typing import Optional
import netsquid as ns
from netsquid.qubits.ketstates import BellIndex
from netsquid_driver.entanglement_tracker_service import LinkStatus, ResLocalDiscard, \
    ResNewLocalLink, ResLocalMeasure, EntanglementTrackerService


class LocalLinkTracker(EntanglementTrackerService):
    """Tracks elementary-link entangled states that are created at this node.

    This entanglement tracker is unable to track global entanglement.
    It is unable to process updates about new entangled states, entanglement swaps and discards away from this node.

    The purpose of this local-link tracker is twofold:
    - Track the status (availability) of local qubits that were created as part of an elementary-link entangled state.
    - Track the memory position that holds the qubit that was created as part of an elementary-link entangled state.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional
        The name of this protocol.

    """

    @dataclass
    class _LocalLinkInfo:
        """Used to keep track of all known information about registered local links."""
        status: LinkStatus
        time_of_birth: Optional[float] = None
        bell_index: Optional[BellIndex] = None
        mem_pos: Optional[int] = None
        time_of_death: Optional[float] = None  # time when swapped or discarded

    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)

        # dictionary used to track information about local links:
        self._local_links = {}  # keys: LinkIdentifier, values: _LocalLinkInfo
        self._available_links = []  # used to speed up getting available links

        # dictionary used to track what link (if any) is stored at a memory position:
        self._mem_positions = {}  # keys: memory position (int), values: LinkIdentifier or None

        # note: in principle, self._mem_positions is unneeded because _LocalLinkInfo contains mem_pos information
        # however, keeping this information in a separate dictionary (which will typically be small) saves
        # some computations

    @property
    def num_available_links(self):
        """Number of usable elementary-link entangled states currently available at this node.

        Returns
        -------
        list of :class:`entanglement_tracker_service.LinkIdentifier`
            All known link identifiers at this node with the status
            :atr:`entanglement_tracker_service.LinkStatus.AVAILABLE`.

        """
        return len(self.get_links_by_status(LinkStatus.AVAILABLE))

    def register_new_link(self, link_identifier, bell_index=None, mem_pos=None, times_of_birth=None):
        """Register the creation of a new entangled state on an elementary link.

        The entangled state must be local to the node that this service runs on
        (i.e. one of the qubits is located at the node), as LocalLinkTracker is only meant for tracking entanglement
        on the elementary links connected to this node.

        If a link is registered for the second time, any information (i.e. `bell_index`, `mem_pos` or `time_of_birth`)
        that is provided that was previously not tracked (i.e. registered as `None`) will be tracked from now on.
        The new information must be consistent with all previously tracked information.

        Parameters
        ----------
        link_identifier: :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the newly-generated elementary link.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Index of the Bell state that was generated.
            If None, the Bell index is not tracked.
        mem_pos : int or None
            Memory position at which the part of the new entangled state located at this node is stored.
            If None, the memory position of this link will not be tracked.
        times_of_birth : dict or float or None
            Times at which the qubits of the elementary-link entangled state indicated by `link_identifier`
            came into existence.
            Dictionary should have the :prop:`netsquid.nodes.node.Node.ID` (int) as keys
            (to refer to specific qubits in the elementary-link entangled state),
            and float or None as values (the time at which the corresponding qubit came into existence).
            If float or None is passed instead of a dictionary, this is used as the time of birth for all qubits
            in the elementary-link entangled state.
            When None is used as a time of birth, the time of birth is not tracked.

        Raises
        ------
        ValueError
            If `link_identifier` is not local to the node this service runs on, i.e.
            if `link_identifier.node_ids` does not contain the local node's :atr:`netsquid.nodes.node.Node.ID`.
        RunTimeError
            If a `link_identifier` is registered that has already been registered before, and the registered
            information (`bell_index`, `mem_pos` or `times_of_birth`) is inconsistent with previously tracked
            information about the link.

        """
        super().register_new_link(link_identifier=link_identifier,
                                  bell_index=bell_index,
                                  mem_pos=mem_pos,
                                  times_of_birth=times_of_birth)
        self._check_local(link_identifier)
        local_time_of_birth = times_of_birth[self.node.ID] if isinstance(times_of_birth, dict) else times_of_birth
        if link_identifier not in self._local_links:
            self._local_links[link_identifier] = self._LocalLinkInfo(status=LinkStatus.AVAILABLE,
                                                                     bell_index=bell_index,
                                                                     mem_pos=mem_pos,
                                                                     time_of_birth=local_time_of_birth)
            self._available_links.append(link_identifier)
            if mem_pos is not None:
                if self._mem_positions.get(mem_pos) is not None:
                    raise RuntimeError(f"Overwriting existing local link. At node: {self.node.name}"
                                       f"link information: {self._local_links[link_identifier]} is overwriting:"
                                       f" {self._local_links[self._mem_positions[mem_pos]]}")
                self._mem_positions[mem_pos] = link_identifier
            self.send_response(ResNewLocalLink(link_identifier=link_identifier,
                                               memory_position=mem_pos))
        else:
            # link was already registered, just check if the bell index, mem_pos or time of birth can be updated
            link_info = self._local_links[link_identifier]
            if link_info.mem_pos is None and mem_pos is not None:
                # start tracking memory position if it was not tracked before
                link_info.mem_pos = mem_pos
            elif link_info.mem_pos is not None and mem_pos is not None:
                if self.get_mem_pos(link_identifier) != mem_pos:
                    raise RuntimeError("Entanglement tracker has received inconsistent information"
                                       "about the memory position at which an elementary-link entangled qubit"
                                       "is stored.")
            if link_info.bell_index is None and bell_index is not None:
                # start tracking Bell index if it was not tracked before
                link_info.bell_index = bell_index
            elif link_info.bell_index is not None and bell_index is not None:
                if bell_index != link_info.bell_index:
                    raise RuntimeError("Entanglement tracker has received inconsistent information"
                                       "about bell indices of elementary-link entangled states.")
            if link_info.time_of_birth is None and local_time_of_birth is not None:
                # start tracking time of birth if it was not tracked before
                link_info.time_of_birth = local_time_of_birth
            elif link_info.time_of_birth is not None and local_time_of_birth is not None:
                if link_info.time_of_birth != local_time_of_birth:
                    raise RuntimeError("Entanglement tracker has received inconsistent information"
                                       "about the time of birth of an elementary-link entangled qubit.")

    def register_new_local_link(self, remote_node_id, bell_index=None, mem_pos=None, time_of_birth=None):
        """Register the creation of a new entangled state on an elementary link at this node.

        This function automatically generates a unique :class:`entanglement_tracker_service.LinkIdentifier`
        and then calls :meth:`entanglement_tracker_service.EntanglementTrackerService`.

        Parameters
        ----------
        remote_node_id : int
            :prop:`netsquid.nodes.node.Node.ID` of the node that this node has generated entanglement with.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Index of the Bell state that was generated.
            If None, the Bell index is not tracked.
        mem_pos : int or None
            Memory position at which the part of the new entangled state located at this node is stored.
            If None, the memory position of this link will not be tracked.
            If the node this service runs on does not share in the entanglement, it should be None.
        time_of_birth : float or None
            Time at which the link was generated.
            If None, the time at which this method is called is used instead.

        """
        super().register_new_local_link(remote_node_id=remote_node_id,
                                        bell_index=bell_index,
                                        mem_pos=mem_pos,
                                        time_of_birth=time_of_birth)

    def register_swap(self, link_identifier_1, link_identifier_2, bell_index, time_of_swap=None):
        """Register the performance of an entanglement swap at this node.

        The entanglement swap must have taken place at this node, since LocalLinkTracker is only meant for
        locally tracking entangled states.

        Parameters
        ----------
        link_identifier_1 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of one of the two elementary-link states that were swapped.
        link_identifier_2 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the other elementary-link state that was swapped.
            `link_identifier_2.node_ids` must have a single node ID overlap with `link_identifier_1.node_ids`.
            It is assumed the swap took place at the node with this ID.
            Since the swap must have taken place locally, it is required that the ID of the overlap node is the ID
            of the node this protocol runs on.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex`
            Bell index of the outcome of the Bell-state measurement.
        time_of_swap : float or None
            Time at which the swap was performed.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If `link_identifier_1` or `link_identifier_2` is not local to the node this service runs on, i.e.
            if `link_identifier_1`.node_ids` or `link_identifier_2.node_ids`
            does not contain the local node's :atr:`netsquid.nodes.node.Node.ID`.
            If `link_identifier_1` or `link_identifier_2` has not been registered at this service.

        """
        super().register_swap(link_identifier_1=link_identifier_1,
                              link_identifier_2=link_identifier_2,
                              bell_index=bell_index,
                              time_of_swap=time_of_swap)
        if time_of_swap is None:
            time_of_swap = ns.sim_time()
        for link_identifier in [link_identifier_1, link_identifier_2]:
            self._check_local(link_identifier)
            self._check_registered(link_identifier)
            local_link_info = self._local_links[link_identifier]
            if local_link_info.status is not LinkStatus.AVAILABLE:
                raise ValueError("The link {link_identifier} is not available and can thus not be swapped.")
            local_link_info.status = LinkStatus.MEASURED
            self._available_links.remove(link_identifier)
            local_link_info.time_of_death = time_of_swap
            mem_pos = local_link_info.mem_pos
            if mem_pos is not None:
                self._mem_positions[mem_pos] = None
            self.send_response(ResLocalMeasure(link_identifier=link_identifier,
                                               memory_position=mem_pos))

    def register_local_swap_mem_pos(self, mem_pos_1, mem_pos_2, bell_index, time_of_swap=None):
        """Register the performance of an entanglement swap at this node using memory positions.

        Note
        ----
        This function tries to match the memory positions to
        :class:`entanglement_tracker_service.LinkIdentifier`s,
        using meth:`local_link_tracker.LocalLinkTracker.get_link`.
        If this is successful, :meth:`local_link_tracker.LocalLinkTracker.register_local_swap`
        is called.
        If no :class:`entanglement_tracker_service.LinkIdentifier` is found for any of the memory
        positions, this method does nothing. No error is raised, since it might be that the tracking functionality
        is simply not used.
        However, if no  :class:`entanglement_tracker_service.LinkIdentifier` is found for one of the memory
        positions, but it is for the other, a ValueError is raised because this suggests something went wrong.

        Parameters
        ----------
        mem_pos_1 : int
            One of the memory positions that the swap was performed on.
        mem_pos_2 : int
            The other memory position that the swap was performed on.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex`
            Bell index of the outcome of the Bell-state measurement.
        time_of_swap : float or None
            Time at which the swap was performed.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If an :class:`entanglement_tracker_service.LinkIdentifier` is found for one of the memory positions
            but not the other.

        """
        super().register_local_swap_mem_pos(mem_pos_1=mem_pos_1,
                                            mem_pos_2=mem_pos_2,
                                            bell_index=bell_index,
                                            time_of_swap=time_of_swap)

    def register_discard(self, link_identifier, node_id=None, time_of_discard=None):
        """Register the discard of an entangled qubit.

        The discard must have taken place at this node, since LocalLinkTracker is only meant for locally tracking
        entangled states.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which the local part was discarded.
        node_id : int or None
            :atr:`netsquid.nodes.node.Node.ID` of the node at which the discard has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_discard : float or None
            Time at which the qubit was discarded.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If `node_id` is not the local node's :prop:`netsquid.nodes.node.Node.ID`.
            If `link_idenetifier.node_ids` does not contain `node_id`.
            If `link_identifier` has not been registered at this service.

        """
        if node_id is None:
            node_id = self.node.ID
        if node_id != self.node.ID:
            raise ValueError(f"{node_id} is not the ID of the node that this service runs on.")
        super().register_discard(link_identifier=link_identifier,
                                 node_id=node_id,
                                 time_of_discard=time_of_discard)
        self._check_local(link_identifier)
        self._check_registered(link_identifier)
        if time_of_discard is None:
            time_of_discard = ns.sim_time()
        local_link_info = self._local_links[link_identifier]
        if local_link_info.status is not LinkStatus.AVAILABLE:
            raise ValueError(f"The link {link_identifier} is not available and can thus not be discarded.")
        local_link_info.status = LinkStatus.DISCARDED
        self._available_links.remove(link_identifier)
        local_link_info.time_of_death = time_of_discard
        mem_pos = local_link_info.mem_pos
        if mem_pos is not None:
            self._mem_positions[mem_pos] = None
        self.send_response(ResLocalDiscard(link_identifier=link_identifier,
                                           memory_position=mem_pos))

    def register_local_discard_mem_pos(self, mem_pos, time_of_discard=None):
        """Register the discard of a qubit at a specific memory position.

        This function tries to match the memory position to a
        :class:`entanglement_tracker_service.LinkIdentifier`,
        using meth:`local_link_tracker.LocalLinkTracker.get_link`.
        If this is successful,
        :meth:`local_link_tracker.LocalLinkTracker.register_local_discard` is called.
        However, if no  :class:`entanglement_tracker_service.LinkIdentifier` is found,
        this method does nothing.

        Parameters
        ----------
        mem_pos : int
            Memory position of the qubit that was discarded.
        time_of_discard : float
            Time at which the qubit was discarded.
            If None, the time at which this request is issued is used instead.

        """
        super().register_local_discard_mem_pos(mem_pos=mem_pos,
                                               time_of_discard=time_of_discard)

    def register_measurement(self, link_identifier, node_id=None, time_of_measurement=None):
        """Register the measurement of a qubit that was created as part of a specific elementary-link entangled state.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state as part of which the measured qubit was created.
        node_id : int or None
            :atr:`netsquid.nodes.node.Node.ID` of the node at which the measurement has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_measurement : float or None
            Time at which the qubit was measured.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If `node_id` is not the local node's :prop:`netsquid.nodes.node.Node.ID`.
            If `link_identifier.node_ids` does not contain `node_id`.
            If `link_identifier` has not been registered at this service.

        """
        if node_id is None:
            node_id = self.node.ID
        if node_id != self.node.ID:
            raise ValueError(f"{node_id} is not the ID of the node that this service runs on.")
        super().register_measurement(link_identifier=link_identifier,
                                     node_id=node_id,
                                     time_of_measurement=time_of_measurement)
        self._check_local(link_identifier)
        self._check_registered(link_identifier)
        if time_of_measurement is None:
            time_of_measurement = ns.sim_time()
        local_link_info = self._local_links[link_identifier]
        if local_link_info.status is not LinkStatus.AVAILABLE:
            raise ValueError(f"The link {link_identifier} is not available and can thus not be measured.")
        local_link_info.status = LinkStatus.MEASURED
        self._available_links.remove(link_identifier)
        local_link_info.time_of_death = time_of_measurement
        mem_pos = local_link_info.mem_pos
        if mem_pos is not None:
            self._mem_positions[mem_pos] = None
        self.send_response(ResLocalMeasure(link_identifier=link_identifier,
                                           memory_position=mem_pos))

    def register_local_measurement_mem_pos(self, mem_pos, time_of_measurement=None):
        """Register the measurement of a local qubit at a specific memory position.

        This function tries to match the memory position to a
        :class:`entanglement_tracker_service.LinkIdentifier`,
        using meth:`local_link_tracker.LocalLinkTracker.get_link`.
        If this is successful,
        :meth:`local_link_tracker.LocalLinkTracker.register_measurement` is called.
        However, if no  :class:`entanglement_tracker_service.LinkIdentifier` is found,
        this method does nothing.

        Parameters
        ----------
        mem_pos : int
            Memory position of the qubit that was measured.
        time_of_measurement : float
            Time at which the qubit was measured.
            If None, the time at which this request is issued is used instead.

        """
        super().register_local_measurement_mem_pos(mem_pos=mem_pos,
                                                   time_of_measurement=time_of_measurement)

    def register_move(self, old_mem_pos, new_mem_pos):
        """Register the move of a qubit from one memory position to another.

        If a elementary-link entangled-state qubit is known to be stored at `old_mem_pos`, this knowledge is changed
        such that it is now known to be stored at `new_mem_pos`.
        If no such qubit is known, this method does nothing.

        Parameters
        ----------
        old_mem_pos : int
            Memory position that held the qubit before the move.
        new_mem_pos : int
            Memory position that holds the qubit after the move.

        """
        link_identifier = self.get_link(old_mem_pos)
        if link_identifier is None:  # there is no link at this memory position
            return
        self._local_links[link_identifier].mem_pos = new_mem_pos
        self._mem_positions[new_mem_pos] = link_identifier
        del self._mem_positions[old_mem_pos]

    def get_status(self, link_identifier):
        """Get the status of local part of an elementary-link entangled state.

        Note that this method is only defined for elementary-link entangled states of which a qubit
        is stored at the node that this service runs on.
        It returns the status of this qubit, which can either be "available", "measured" or "discarded".

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state that the status is requested of.

        Returns
        -------
        :class:`entanglement_tracker_service.LinkStatus` or None
            Status of the elementary-link entangled state.
            If no status is known, None is returned.

        """
        local_link_info = self._local_links.get(link_identifier, None)
        if local_link_info is None:
            return None
        return local_link_info.status

    def get_mem_pos(self, link_identifier):
        """Get the memory position that holds the local part of a specific elementary-link entangled state.

        Note that this method is only defined for elementary-link entangled states of which a qubit
        is stored at the node that this service runs on.
        It returns the memory position of this qubit.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state that the memory position is requested of.

        Returns
        -------
        int or None
            Memory position that holds the qubit that was created as part of the elementary-link entangled state.
            If no memory position is known, or the status of the link is not
            :atr:`entanglement_tracker_service.LinkStatus.AVAILABLE`, None is returned.

        """
        if self.get_status(link_identifier) != LinkStatus.AVAILABLE:
            return None
        return self._local_links[link_identifier].mem_pos

    def get_link(self, mem_pos):
        """Get the identifier of the elementary-link entangled state of which part is stored at this memory position.

        Parameters
        ----------
        mem_pos : int
            Memory position at memory of this node at which to look for a qubit that was created as part of an
            elementary-link entangled state.

        Returns
        -------
        :class:`entanglement_tracker_service.LinkIdentifier` or None
            Identifier of the elementary-link entangled state of which part is stored in the memory position.
            If no such identifier is known, None is returned.

        """
        return self._mem_positions.get(int(mem_pos), None)

    def get_links_by_status(self, link_status):
        """Get all identifiers of local elementary-link entangled states with a specific status.

        Note that since the status of a link is only locally defined (it is the status of a local qubit that was
        created as part of an elementary-link entangled state), this only returns link identifiers corresponding
        to elementary-link entangled states local to the node this service runs on.

        Parameters
        ----------
        link_status : :class:`entanglement_tracker_service.LinkStatus`
            Status for which to find link identifiers.

        Returns
        -------
        list of :class:`entanglement_tracker_service.LinkIdentifier`
            All known link identifiers for which the entangled state has this status.

        """
        link_status = LinkStatus(link_status)  # raises error if it cannot be interpreted as LinkStatus
        if link_status is LinkStatus.AVAILABLE:
            return self._available_links
        else:
            return [link_identifier for link_identifier, link_info in self._local_links.items()
                    if link_info.status == link_status]

    def get_local_link_lifetime(self, link_identifier):
        """Get the lifetime of a qubit that was created locally as part of an elementary-link entangled state.

        Note that this is about local lifetime: it is the amount of time one specific qubit has spent in local memory.
        This method also works for expired links, i.e. those that do not have the status
        :atr:`entanglement_tracker_service.LinkStatus.AVAILABLE`.
        When a link expires, the lifetime is frozen.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which the local qubit's lifetime is requested.

        Returns
        -------
        float or None
            The local lifetime of the qubit.
            None if the time of birth is not tracked.

        Raises
        ------
        ValueError
            If link_identifier has not been registered at this service.

        """
        self._check_local(link_identifier)
        self._check_registered(link_identifier)
        link_info = self._local_links[link_identifier]
        if link_info.time_of_birth is None:
            return None
        end_time = link_info.time_of_death
        end_time = ns.sim_time() if end_time is None else end_time
        return end_time - link_info.time_of_birth

    def get_bell_index(self, link_identifier):
        """Get the Bell index corresponding to an elementary-link entangled state of which one qubit is local.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which the local qubit's lifetime is requested.

        Returns
        -------
        :class:`netsquid.qubits.ketstates.BellIndex` or None
            Index indicating what Bell state the elementary-link entangled state is thought to be.
            If None, the Bell index is not tracked.

        """
        self._check_local(link_identifier)
        self._check_registered(link_identifier)
        return self._local_links[link_identifier].bell_index

    def _check_local(self, link_identifier):
        """Check if an elementary-link entangled state is local to the node this service runs on.

        Throws a ValueError if the link is not local.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of elementary-link entangled state to check for locality.

        Raises
        ------
        ValueError
            If `link_identifier` is not local to the node this service runs on, i.e.
            if `link_identifier.node_ids` does not contain the local node's :prop:`netsquid.nodes.node.Node.ID`.

        """
        if not self.is_local(link_identifier):
            raise ValueError(f"The link {link_identifier} is not local to the node {self.node.name}.")

    def _check_registered(self, link_identifier):
        """Check if an elementary-link entangled state has been registered at this service.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of elementary-link entangled state of which the registration is checked.

        Raises
        ------
        ValueError
            If link_identifier has not been registered at this service.

        """
        if link_identifier not in self._local_links:
            raise ValueError(f"The link {link_identifier} has not yet been registered.")

    def get_entanglement_info_of_link(self, link_identifier):
        """Get entanglement information about a qubit that was created as part of an elementary-link entangled state.

        This method is not implemented for `LocalLinkTracker`, since it only tracks local entanglement.

        The entanglement information reflects what this entanglement tracker believes to be true about global
        entanglement, rather than what is actually true.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state about which the local qubit's entanglement info is
            requested.

        Raises
        ------
        NotImplementedError
            This method is not implemented for `LocalLinkTracker`.

        """
        raise NotImplementedError("LocalLinkTracker does not track information about global entanglement.")

    def get_entanglement_info_between_nodes(self, node_ids, other_nodes_allowed=False, must_be_intact=True):
        """Get all entanglement information about entanglement between specific nodes.

        This method is not implemented for `LocalLinkTracker`, since it only tracks local entanglement.

        Examples
        --------
        1. When Alice wants to know if she shared bipartite entanglement with Bob, e.g. to be used in QKD, she can call
        `get_entanglement_info_between_nodes(node_ids={node_alice.ID, node_bob.ID}, other_nodes_allowed=False)`.

        2. When Charlie wants to know what entanglement she shares with any other nodes in the network, she can call
        `get_entanglement_info_between_nodes(node_ids={node_charlie.ID}, other_nodes_allowed=True)`.

        Parameters
        ----------
        node_ids : set of int
            :prop:`netsquid.nodes.node.Node.ID` of nodes for which entanglement information is requested.
        other_nodes_allowed : bool
            True if the returned entanglement information can be about entangled states which are not just shared
            by the nodes in `node_ids`, but also by additional nodes.
            False if the returned entanglement information must describe entanglement between the nodes in `node_ids`
            and no other nodes.
        must_be_intact : bool
            True if only entanglement information for which `EntanglementInfo.intact == True` should be returned.
            An `EntanglementInfo` that is not "intact" represents an entangled state that is thought to have been
            destroyed somehow, rendering the qubit no longer entangled.

        Raises
        ------
        NotImplementedError
            This method is not implemented for `LocalLinkTracker`.

        """
        raise NotImplementedError("LocalLinkTracker does not track information about global entanglement.")

    def await_entanglement(self, node_ids, link_identifiers=set(), other_nodes_allowed=False):
        """Wait for an entangled state between specific nodes based on specific elementary-link entangled states.

        Wait until the entanglement tracker believes there is an entangled state shared between the required
        nodes, that has been built using the required elementary-link entangled states.
        That is, until there is an `EntanglementInfo` available that includes `node_ids` in
        `EntanglementInfo.entangled_nodes` and `link_identifiers` in `EntanglementInfo.elementary_links`.

        In case the entanglement tracker believes that one of the required elementary-link entangled states
        is no longer part of an intact entangled state, obtaining the target entangled state is deemed impossible
        and the wait is stopped.
        That is, this method waits until either an `EntanglementInfo` is available that fulfills the conditions
        described above, or when there is an `EntanglementInfo` that includes one of the `link_identifiers`
        in `EntanglementInfo.elementary_links` for which `EntanglementInfo.intact == False`.

        To have a protocol await entanglement, put `yield from tracker.await_entanglement()` in the
        `run()` method of the protocol, where `tracker` is this entanglement tracker.

        Examples
        --------
        1. Alice and Bob are continuously generating entanglement through e.g. a repeater chain.
         Alice can consume her entanglement as soon as it becomes available by calling `await_entanglement`
         with `node_ids = {Alice.ID, Bob.ID}`.
         After the wait, she locates the entangled qubit using e.g. `get_entanglement_info_between_nodes`
         and consumes it.
         She can then again start waiting for the next entanglement.

        2. Alice created entanglement with her neighbour, which is part of a repeater chain.
         She now hopes that the repeater chain will entangle her qubit with Bob through entanglement swapping.
         However, one of the repeaters in the chain might also destroy the entangled state when a cutoff time is met.
         To this end, Alice uses `await_entanglement` with `node_ids = {Alice.ID, Bob.ID}` and
         `link_identifiers = {link_with_neighbour_identifier}`.
         After the wait is over, she checks whether her qubit is entangled with Bob, or whether the entanglement was
         destroyed (e.g. by checking the value of `get_entanglement_of_link(link_with_neithbour_identifier).intact`).
         She can then either use her entangled qubit for some application, or try again.

        Parameters
        ----------
        node_ids : set of int
            :prop:`netsquid.nodes.node.Node.ID` of the nodes that should share the entangled state that is waited for.
        link_identifiers : set of LinkIdentifier
            Identifiers of elementary-link states that should have gone into the entangled state that is waited for.
        other_nodes_allowed : bool
            True if the entangled state that is waited for can not only be shared between the nodes in `node_ids`,
            but also by additional nodes.
            False if the entangled state must be just between the nodes in `node_ids` and no other nodes.

        """
        raise NotImplementedError
