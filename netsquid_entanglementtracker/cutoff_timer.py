import logging

from netsquid_entanglementtracker.cutoff_service import CutoffService
from netsquid.protocols.nodeprotocols import NodeProtocol
from netsquid_driver.entanglement_tracker_service import LinkStatus, ResNewLocalLink

logger = logging.getLogger(__name__)


class CutoffTimer(CutoffService):
    """Implementation of a CutoffService based on a timer. When the timer for a link runs out, the link is discarded.

    Whenever a new link is registered, :class:`cutoff_timer.LinkCutoffTimer` is started for the
    particular link identifier. The LinkCutoffTimer runs a timer after which the status of the links is checked,
    and the link is discarded if it is still available.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    cutoff_time : float
        Time in nanoseconds after which all links should be discarded.
    name : str, optional
        Name of this protocol.
    """
    def __init__(self, node, cutoff_time, name=None):
        super().__init__(node=node, name=name)
        self._cutoff_time = cutoff_time
        self.logger = logger.getChild(self.node.name)

    def run(self):
        """Listens to new links that are registered. When that happens, a LinkCutoffTimer is started."""
        while True:
            evt_expr = yield self.await_signal(sender=self.entanglement_tracker, signal_label=ResNewLocalLink.__name__)
            [evt] = evt_expr.triggered_events
            result = self.entanglement_tracker.get_signal_by_event(event=evt).result
            # start cutoff timer for the link that was registered
            timer = LinkCutoffTimer(node=self.node, superprotocol=self, cutoff_time=self._cutoff_time,
                                    link_identifier=result.link_identifier)
            timer.start()


class LinkCutoffTimer(NodeProtocol):
    """Protocol that acts as a cutoff timer for a specific link.

    For a specific link, this protocol is run two times: once from each node on which the new link is registered.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    superprotocol : :class:`cutoff_timer.CutoffTimer`
        Protocol that this protocol is a subprotocol of.
    cutoff_time : float
        Time in nanoseconds after which a link should be discarded.
    link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
        Identifier of the elementary-link entangled state for which the cutoff timer should be started.
    """

    def __init__(self, node, superprotocol, cutoff_time, link_identifier):
        super().__init__(node=node, name=f"LinkCutoffTimer_{node.ID}_{link_identifier.pair_identifier}")
        self._superprotocol = superprotocol
        self._cutoff_time = cutoff_time
        self._link_identifier = link_identifier

    def run(self):
        """A timer is set after which the status of the link is checked.
        If the link is still available, a request to discard it is issued. After that, the protocol is stopped.
        """
        yield self.await_timer(duration=self._cutoff_time)
        if self._superprotocol.entanglement_tracker.get_status(self._link_identifier) is LinkStatus.AVAILABLE:
            self._superprotocol.discard_link(self._link_identifier)
            self._superprotocol.logger.info(f"Discarding link: {self._link_identifier}")
