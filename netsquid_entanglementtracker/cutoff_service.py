from abc import ABCMeta

from netsquid.protocols import ServiceProtocol

from netsquid_driver.entanglement_tracker_service import EntanglementTrackerService


class CutoffService(ServiceProtocol, metaclass=ABCMeta):
    """Service framework for implementations of cutoff.

    This service should be subclassed with specific implementations of cutoff. Those implementations need to take care
    of listening to signals from `entanglement_tracker_service.EntanglementTrackerService` and checking when
    conditions for discarding an elementary-link entangled state are met.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on
    name : str, optional
        The name of this protocol

    """

    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)

    def discard_link(self, link_identifier, node_id=None, time_of_discard=None):
        """Discard an entangled state on a particular elementary link due to cutoff.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which the local part should be discarded.
        node_id : int or None
            :atr:`netsquid.nodes.node.Node.ID` of the node at which the discard takes place.
            If None, the ID of the node this service runs on is used.
        time_of_discard : float or None
            Time at which the qubit was discarded.
            If None, the time of issuing the discard request is used instead.
        """
        self.entanglement_tracker.register_discard(link_identifier, node_id=node_id, time_of_discard=time_of_discard)

    @property
    def entanglement_tracker(self):
        """The local entanglement tracker, if there is any.

        Returns
        -------
        :class:`entanglement_tracker_service.EntanglementTrackerService` or None
            Entanglement tracker if there is any, otherwise None.

        """
        entanglement_tracker = None
        try:
            entanglement_tracker = self.node.driver[EntanglementTrackerService]
        except (AttributeError, KeyError):
            pass
        return entanglement_tracker

    @property
    def is_connected(self):
        """The protocol is only connected if the node also has a :class:`Driver` attached to it,
        with a registered :class:`entanglement_tracker_service.EntanglementTrackerService`."""
        if self.entanglement_tracker is None:
            return False
        return super().is_connected
