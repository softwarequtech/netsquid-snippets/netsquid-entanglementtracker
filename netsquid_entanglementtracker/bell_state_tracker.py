from dataclasses import dataclass
import netsquid as ns

from netsquid_driver.classical_routing_service import ClassicalRoutingService, RemoteServiceRequest
from netsquid_entanglementtracker.local_link_tracker import LocalLinkTracker
from netsquid_driver.entanglement_tracker_service import BellStateEntanglementInfo, ReqNewLinkUpdate, \
    ReqSwapUpdate, ReqDiscardUpdate, LinkStatus, EntanglementTrackerService, ReqMeasureUpdate


class BellStateTracker(LocalLinkTracker):
    """Tracks Bell states in a quantum network.

    This entanglement tracker both tracks local qubits that are created as part of an elementary-link entangled state
    (using functionality of :class:`local_link_tracker.LocalLinkTracker`)
    and global entanglement in the network, as long as the entangled states are always Bell states.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional
        The name of this protocol.
    untrack_stale_entanglement : bool, optional
        If True, entanglement is no longer tracked when it becomes "stale".
        Entanglement is considered "stale" when it has no alive qubits (i.e. all qubits have been measured or discarded)
        and none of the entangled qubits are located at the node this protocol is running on.
        The purpose of this is to avoid the entanglement tracker from slowing down as the number of tracked
        entangled states grows (see also Notes below).

    Notes
    -----
    This entanglement tracker can become very slow for long simulations, because the database of tracked entanglement
    keeps growing.
    The database has to be searched through often (especially in `BellStateTracker.register_swap()`),
    and the time required for each search scales as O(n), where n is the number of entangled states that are tracked.
    For repeater-chain simulations, this has previously resulted in O(m^2) scaling of the total simulation time,
    where m is the number of end-to-end entangled states that are distributed.
    This is caused by the fact that n grows as O(m), but also the total number of times the database is searched through
    grows as O(m), assuming the number of searches required per end-to-end entangled state is more or less constant.

    This superlinear scaling can be counteracted by cleaning up the database.
    In part, this is done automatically if `untrack_stale_entanglement` is True.
    However, even then, entangled states with qubits at the node this service runs on keep being tracked,
    even after measurement or discard (since the information about the entanglement can often still be useful, as
    e.g. in the example below).
    To also untrack such entangled states, calls to `BellStateTracker.untrack_entanglement_info()` can be made.

    For example, an EGP service handling a measure-directly request will typically measure out a local entangled qubit
    before end-to-end entanglement has been confirmed.
    As soon as end-to-end entanglement is confirmed, the service will want to know the corresponding Bell index.
    Since the remote node will also have measured its qubit directly, the entanglement does not have any alive qubits.
    If all entanglement without alive qubits would have been untracked automatically,
    it would now be impossible for the service to learn the Bell index.
    This is why entanglement is never considered "stale" if it involves local qubits.
    Because the entangled state is still being tracked. the service can retrieve the corresponding `EntanglementInfo`
    and learn the bell index.
    Then, in order to keep the database clean, the service should call `BellStateTracker.untrack_entnaglement_info`
    on the retrieved `EntanglementInfo`.

    """

    @dataclass
    class _EntanglementOrder:
        node_ids: set
        link_identifiers: set
        other_nodes_allowed: bool

    def __init__(self, node, name=None, untrack_stale_entanglement=True):
        super().__init__(node=node, name=name)
        self._untrack_stale_entanglement = untrack_stale_entanglement
        self._entanglement_info_to_be_untracked = []
        self._entanglement_info = []
        self._entanglement_info_of_link = {}
        self._entanglement_orders = []
        self._signal_entanglement_order_fulfilled = "Entanglement order has been fulfilled."
        self.add_signal(self._signal_entanglement_order_fulfilled)

    @property
    def untrack_stale_entanglement(self):
        """" Whether "stale" (i.e. nonlocal and without alive qubits) entanglement should be untracked automatically.

        If True, entanglement is no longer tracked when it becomes "stale".
        Entanglement is considered "stale" when it has no alive qubits (i.e. all qubits have been measured or discarded)
        and none of the entangled qubits are located at the node this protocol is running on.
        The purpose of this is to avoid the entanglement tracker from slowing down as the number of tracked
        entangled states grows.

        Returns
        -------
        bool : True if stale entanglement is automatically discarded.

        """
        return self._untrack_stale_entanglement

    def register_new_link(self, link_identifier, bell_index=None, mem_pos=None, times_of_birth=None):
        """Register the creation of a new entangled state on an elementary link.

        The entangled state can be either local or nonlocal.
        If it is local, and the link identifier is not yet known locally, the information registered
        here is considered novel and broadcasted for other entanglement trackers to pick up.
        If it is local and the link identifier had already been registered,
        it is checked whether any information about the entangled state (Bell index, memory position or time of birth)
        can be updated with the provided information.
        If this is so, the information is also considered novel and therefore broadcasted.

        Whether the entangled state is local or nonlocal, the information is always used to update information
        about global entanglement if possible.

        If a link is registered for the second time, any information (i.e. `bell_index`, `mem_pos` or `times_of_birth`)
        that is provided that was previously not tracked (i.e. registered as `None`) will be tracked from now on.
        The new information must be consistent with all previously tracked information.

        Parameters
        ----------
        link_identifier: :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the newly-generated elementary link.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Index of the Bell state that was generated.
            If None, the Bell index is not tracked.
        mem_pos : int or None
            Memory position at which the part of the new entangled state located at this node is stored.
            If None, the memory position of this link will not be tracked.
            If the newly-generated elementary link is not local to the node this protocol runs on,
            the memory position will not be tracked either way and this argument will be ignored.
        times_of_birth : dict or float or None
            Times at which the qubits of the elementary-link entangled state indicated by `link_identifier`
            came into existence.
            Dictionary should have the :prop:`netsquid.nodes.node.Node.ID` (int) as keys
            (to refer to specific qubits in the elementary-link entangled state),
            and float or None as values (the time at which the corresponding qubit came into existence).
            If float or None is passed instead of a dictionary, this is used as the time of birth for all qubits
            in the elementary-link entangled state.
            When None is used as a time of birth, the time of birth is not tracked.

        Raises
        ------
        RunTimeError
            If a `link_identifier` is registered that has already been registered before, and the registered
            information (`bell_index`, `mem_pos` or `times_of_birth`) is inconsistent with previously tracked
            information about the link.

        """
        if not isinstance(times_of_birth, dict):
            times_of_birth = {node_id: times_of_birth for node_id in link_identifier.node_ids}
        update_contains_new_local_info = False
        update_contains_new_global_info = False
        if self.is_local(link_identifier):
            if link_identifier not in self._local_links:
                # the link was not yet registered
                update_contains_new_local_info = True
            else:
                if self.get_bell_index(link_identifier) is None and bell_index is not None:
                    update_contains_new_local_info = True
                if self.get_local_link_lifetime(link_identifier) is None and times_of_birth[self.node.ID] is not None:
                    update_contains_new_local_info = True
            super().register_new_link(link_identifier=link_identifier,
                                      bell_index=bell_index,
                                      mem_pos=mem_pos,
                                      times_of_birth=times_of_birth)
            if update_contains_new_local_info:
                self._broadcast_new_link_update(link_identifier=link_identifier,
                                                bell_index=bell_index,
                                                times_of_birth=times_of_birth)

        # in both local and nonlocal case, global entanglement is updated
        entanglement_info = self.get_entanglement_info_of_link(link_identifier)
        if entanglement_info is None:
            new_entanglement_info = BellStateEntanglementInfo(link_identifier=link_identifier,
                                                              bell_index=bell_index,
                                                              times_of_birth=times_of_birth)
            self._entanglement_info_of_link[link_identifier] = new_entanglement_info
            self._entanglement_info.append(new_entanglement_info)
            entanglement_info = new_entanglement_info
            update_contains_new_global_info = True
        else:
            previously_known_bell_index = entanglement_info.get_link_bell_index(link_identifier=link_identifier)
            if previously_known_bell_index is None and bell_index is not None:
                entanglement_info.set_link_bell_index(link_identifier=link_identifier,
                                                      bell_index=bell_index)
                update_contains_new_global_info = True
            elif previously_known_bell_index is not None and bell_index is not None:
                if entanglement_info.get_link_bell_index(link_identifier=link_identifier) != bell_index:
                    raise RuntimeError("Entanglement tracker has received inconsistent information"
                                       "about bell indices of elementary-link entangled states.")

            for node_id in link_identifier.node_ids:
                previously_known_time_of_birth = entanglement_info.get_time_of_birth(link_identifier=link_identifier,
                                                                                     node_id=node_id)
                if previously_known_time_of_birth is None and times_of_birth[node_id] is not None:
                    entanglement_info.set_time_of_birth(link_identifier=link_identifier,
                                                        node_id=node_id,
                                                        time_of_birth=times_of_birth[node_id])
                    update_contains_new_global_info = True
                elif previously_known_time_of_birth is not None and times_of_birth[node_id] is not None:
                    # check if there is no conflict in information
                    if previously_known_time_of_birth != times_of_birth[node_id]:
                        raise RuntimeError("Entanglement tracker has received inconsistent information"
                                           "about times of birth of elementary-link entangled qubits.")

        if update_contains_new_global_info:
            self._process_entanglement_update(entanglement_info)

    def _broadcast_new_link_update(self, link_identifier, bell_index, times_of_birth):
        """Broadcast information about a new elementary-link entangled state through the network.

        Parameters
        ----------
        link_identifier: :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the newly-generated elementary link.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex`
            Index of the Bell state that was generated.
        times_of_birth : dict or float or None
            Times at which the qubits of the elementary-link entangled state indicated by `link_identifier`
            came into existence.
            Dictionary should have the :prop:`netsquid.nodes.node.Node.ID` (int) as keys
            (to refer to specific qubits in the elementary-link entangled state),
            and float or None as values (the time at which the corresponding qubit came into existence).
            If float or None is passed instead of a dictionary, this is used as the time of birth for all qubits
            in the elementary-link entangled state.
            When None is used as a time of birth, the time of birth is not tracked.

        """
        req = ReqNewLinkUpdate(link_identifier=link_identifier,
                               bell_index=bell_index,
                               times_of_birth=times_of_birth)
        self._broadcast_request(req)

    def register_swap(self, link_identifier_1, link_identifier_2, bell_index=None, time_of_swap=None):
        """Register that an entanglement swap has been performed.

        The swap can either have taken place locally at the node this service runs on,
        or at some other node.
        If it is local, the information registered is considered novel and broadcasted for other entanglement trackers
        to pick up.
        Underlying this is the assumption that swaps are always only broadcasted by the nodes at which they take place.

        After updating entanglement information accordingly, it is checked if any locally available qubit is part of
        an entangled state that is believed to be not intact (i.e. of which the entanglement has been somehow destroyed,
        e.g. through the discarding of an entangled qubit).
        This can happen if an intact entangled state is swapped with a non-intact entangled state.
        Each qubit that is believed to be part of a non-intact entangled state is discarded.

        If a local swap is registered between elementary-link entangled states that have not yet been registered,
        this will raise an error.
        However, if the swap is nonlocal, the elementary-link entangled states will be registered on the go
        (but without tracking any information about the state, i.e. without tracking the bell index or time of birth).
        This is to avoid errors when messages containing updates, broadcasted by other entanglement trackers,
        do no arrive in the expected order.
        Note that when the new-link updates do arrive, any additional information in these messages will be added
        to the entanglement tracker.

        Parameters
        ----------
        link_identifier_1 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of one of the two elementary-link states that were swapped.
        link_identifier_2 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the other elementary-link state that was swapped.
            `link_identifier_2.node_ids` must have a single node ID overlap with `link_identifier_1.node_ids`.
            It is assumed the swap took place at the node with this ID.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex` or None
            Bell index of the outcome of the Bell-state measurement.
            If None, Bell index is not tracked.
        time_of_swap : float or None
            Time at which the swap was performed.
            If None, the time at which this request is issued is used instead.

        """
        if time_of_swap is None:
            time_of_swap = ns.sim_time()
        overlap_node_ids = [node_id for node_id in link_identifier_1.node_ids if node_id in link_identifier_2.node_ids]
        if len(overlap_node_ids) != 1:
            raise ValueError("link identifiers used to register swap have no overlapping node.")
        overlap_node_id = overlap_node_ids[0]
        if overlap_node_id == self.node.ID:
            super().register_swap(link_identifier_1=link_identifier_1,
                                  link_identifier_2=link_identifier_2,
                                  bell_index=bell_index,
                                  time_of_swap=time_of_swap)
            self._broadcast_swap_update(link_identifier_1=link_identifier_1,
                                        link_identifier_2=link_identifier_2,
                                        bell_index=bell_index,
                                        time_of_swap=time_of_swap)
        for link_identifier in [link_identifier_1, link_identifier_2]:
            if self.get_entanglement_info_of_link(link_identifier) is None:
                self.register_new_link(link_identifier=link_identifier,
                                       bell_index=None,
                                       mem_pos=None,
                                       times_of_birth=None)
        entanglement_info_1 = self.get_entanglement_info_of_link(link_identifier_1)
        entanglement_info_2 = self.get_entanglement_info_of_link(link_identifier_2)
        new_entanglement_info = entanglement_info_1.merge_by_swap(other_entanglement_info=entanglement_info_2,
                                                                  link_identifier_1=link_identifier_1,
                                                                  link_identifier_2=link_identifier_2,
                                                                  bell_index=bell_index,
                                                                  time_of_swap=time_of_swap)
        for link_identifier in new_entanglement_info.elementary_links:
            self._entanglement_info_of_link[link_identifier] = new_entanglement_info
        self._entanglement_info.remove(entanglement_info_1)
        if entanglement_info_1 in self._entanglement_info_to_be_untracked:
            self._entanglement_info_to_be_untracked.remove(entanglement_info_1)
        self._entanglement_info.remove(entanglement_info_2)
        if entanglement_info_2 in self._entanglement_info_to_be_untracked:
            self._entanglement_info_to_be_untracked.remove(entanglement_info_2)
        self._entanglement_info.append(new_entanglement_info)
        self._discard_qubits_if_entangled_state_not_intact()
        self._process_entanglement_update(new_entanglement_info)
        if self.untrack_stale_entanglement and self._entanglement_is_stale(new_entanglement_info):
            self.untrack_entanglement_info(new_entanglement_info)

    def _broadcast_swap_update(self, link_identifier_1, link_identifier_2, bell_index, time_of_swap):
        """Broadcast information about an entanglement swap through the network.

        Parameters
        ----------
        link_identifier_1 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of one of the two elementary-link states that were swapped.
        link_identifier_2 : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the other elementary-link state that was swapped.
        bell_index : :class:`netsquid.qubits.ketstates.BellIndex`
            Bell index of the outcome of the Bell-state measurement.
        time_of_swap : float
            Time at which the swap was performed.

        """
        req = ReqSwapUpdate(link_identifier_1=link_identifier_1,
                            link_identifier_2=link_identifier_2,
                            bell_index=bell_index,
                            time_of_swap=time_of_swap)
        self._broadcast_request(req)

    def register_discard(self, link_identifier, node_id=None, time_of_discard=None):
        """Register the discard of a qubit that was created as part of a specific elementary-link entangled state.

        The discard can either have taken place locally at the node this service runs on,
        or at some other node.
        If it is local, the information registered here is considered novel and broadcasted for other entanglement
        trackers to pick up.
        Underlying this is the assumption that discards are always only broadcasted by the nodes at which they take
        place.

        If the discarded qubit was entangled with a local qubit, the local qubit is also discarded.

        If a discard is registered for an elementary-link entangled state that is partially local but has not yet been
        registered, this will raise an error.
        However, if the discarded state is nonlocal, the elementary-link entangled state will be registered on the go
        (but without tracking any information about the state, i.e. without tracking the bell index or time of birth).
        This is to avoid errors when messages containing updates, broadcasted by other entanglement trackers,
        do no arrive in the expected order.
        Note that when the new-link updates do arrive, any additional information in these messages will be added
        to the entanglement tracker.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state as part of which the discarded qubit was created.
        node_id : int or None
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the discard has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_discard : float or None
            Time at which the qubit was discarded.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If `link_identifier.node_ids` does not contain `node_id`.

        """
        if node_id is None:
            node_id = self.node.ID
        if node_id not in link_identifier.node_ids:
            raise ValueError(f"node ID {node_id} not in link_identifier.node_ids")
        if time_of_discard is None:
            time_of_discard = ns.sim_time()
        if node_id == self.node.ID:
            super().register_discard(link_identifier=link_identifier, node_id=node_id, time_of_discard=time_of_discard)
            self._broadcast_discard_update(link_identifier=link_identifier,
                                           node_id=node_id,
                                           time_of_discard=time_of_discard)
        if self.get_entanglement_info_of_link(link_identifier) is None and self.node.ID not in link_identifier.node_ids:
            self.register_new_link(link_identifier=link_identifier,
                                   bell_index=None,
                                   mem_pos=None,
                                   times_of_birth=None)
        entanglement_info = self.get_entanglement_info_of_link(link_identifier)
        entanglement_info.intact = False
        entanglement_info.set_time_of_death(link_identifier=link_identifier,
                                            node_id=node_id,
                                            time_of_death=time_of_discard)
        self._discard_qubits_if_entangled_state_not_intact()
        self._process_entanglement_update(entanglement_info)
        if self.untrack_stale_entanglement and self._entanglement_is_stale(entanglement_info):
            self.untrack_entanglement_info(entanglement_info)
        if entanglement_info in self._entanglement_info_to_be_untracked:
            self.untrack_entanglement_info(entanglement_info)

    def _broadcast_discard_update(self, link_identifier, node_id, time_of_discard):
        """Broadcast information about a discard through the network.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state of which the local part was discarded.
        node_id : int or None
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the discard has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_discard : float or None
            Time at which the qubit was discarded.
            If None, the time at which this request is issued is used instead.

        """
        req = ReqDiscardUpdate(link_identifier=link_identifier,
                               node_id=node_id,
                               time_of_discard=time_of_discard)
        self._broadcast_request(req)

    def _discard_qubits_if_entangled_state_not_intact(self):
        """Discard all local, available qubits that are thought to be part of non-intact entangled states.

        Entangled states are considered non-intact if the entanglement has somehow been destroyed.
        This is e.g. the case when one of the entangled qubits has been discarded.

        """
        for local_link in self.get_links_by_status(LinkStatus.AVAILABLE):
            entanglement_info = self.get_entanglement_info_of_link(local_link)
            if not entanglement_info.intact:
                self.register_discard(link_identifier=local_link,
                                      node_id=self.node.ID,
                                      time_of_discard=ns.sim_time())

    def register_measurement(self, link_identifier, node_id=None, time_of_measurement=None):
        """Register the measurement of an entangled qubit.

        The measurement can either have taken place locally at the node this service runs on,
        or at some other node.
        If it is local, the information registered here is considered novel and broadcasted for other entanglement
        trackers to pick up.
        Underlying this is the assumption that measurements are always only broadcasted by the nodes at which they take
        place.

        If a nonlocal measurement is registered for an elementary-link entangled state that has not yet been
        registered, this will raise an error.
        However, if the measurement is nonlocal, the elementary-link entangled state will be registered on the go
        (but without tracking any information about the state, i.e. without tracking the bell index or time of birth).
        This is to avoid errors when messages containing updates, broadcasted by other entanglement trackers,
        do no arrive in the expected order.
        Note that when the new-link updates do arrive, any additional information in these messages will be added
        to the entanglement tracker.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state as part of which the measured qubit was created.
        node_id : int or None
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the measurement has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_measurement : float or None
            Time at which the qubit was measured.
            If None, the time at which this request is issued is used instead.

        Raises
        ------
        ValueError
            If `link_identifier.node_ids` does not contain `node_id`.

        """
        if node_id is None:
            node_id = self.node.ID
        if node_id not in link_identifier.node_ids:
            raise ValueError(f"node ID {node_id} not in link_identifier.node_ids")
        if time_of_measurement is None:
            time_of_measurement = ns.sim_time()
        if node_id == self.node.ID:
            super().register_measurement(link_identifier=link_identifier,
                                         node_id=node_id,
                                         time_of_measurement=time_of_measurement)
            self._broadcast_measure_update(link_identifier=link_identifier,
                                           node_id=node_id,
                                           time_of_measurement=time_of_measurement)
        if self.get_entanglement_info_of_link(link_identifier) is None:
            self.register_new_link(link_identifier=link_identifier,
                                   bell_index=None,
                                   mem_pos=None,
                                   times_of_birth=None)
        entanglement_info = self.get_entanglement_info_of_link(link_identifier)
        entanglement_info.set_time_of_death(node_id=node_id,
                                            link_identifier=link_identifier,
                                            time_of_death=time_of_measurement)
        self._process_entanglement_update(entanglement_info)
        if self.untrack_stale_entanglement and self._entanglement_is_stale(entanglement_info):
            self.untrack_entanglement_info(entanglement_info)
        if entanglement_info in self._entanglement_info_to_be_untracked:
            self.untrack_entanglement_info(entanglement_info)

    def _broadcast_measure_update(self, link_identifier, node_id, time_of_measurement):
        """Broadcast information about a measurement through the network.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state as part of which the measured qubit was created.
        node_id : int or None
            :prop:`netsquid.nodes.node.Node.ID` of the node at which the measurement has taken place.
            If None, the ID of the node this service runs on is used.
        time_of_measurement : float or None
            Time at which the qubit was measured.
            If None, the time at which this request is issued is used instead.

        """
        req = ReqMeasureUpdate(link_identifier=link_identifier,
                               node_id=node_id,
                               time_of_measurement=time_of_measurement)
        self._broadcast_request(req)

    def get_entanglement_info_of_link(self, link_identifier):
        """Get entanglement information about a qubit that was created as part of an elementary-link entangled state.

        The entanglement information reflects what this entanglement tracker believes to be true about global
        entanglement, rather than what is actually true.

        Parameters
        ----------
        link_identifier : :class:`entanglement_tracker_service.LinkIdentifier`
            Identifier of the elementary-link entangled state about which the local qubit's entanglement info is
            requested.

        Returns
        -------
        :class:`entanglement_tracker_service.EntanglementInfo` or None
            Holder of information about the entangled state that the qubit is part of.
            None if `link_identifier` has not been registered yet.

        """
        if link_identifier in self._entanglement_info_of_link:
            return self._entanglement_info_of_link[link_identifier]
        else:
            return None

    def get_entanglement_info_between_nodes(self, node_ids, other_nodes_allowed=False, must_be_intact=True):
        """Get all entanglement information about entanglement between specific nodes.

        Examples
        --------
        1. When Alice wants to know if she shared bipartite entanglement with Bob, e.g. to be used in QKD, she can call
        `get_entanglement_info_between_nodes(node_ids={node_alice.ID, node_bob.ID}, other_nodes_allowed=False)`.

        2. When Charlie wants to know what entanglement she shares with any other nodes in the network, she can call
        `get_entanglement_info_between_nodes(node_ids={node_charlie.ID}, other_nodes_allowed=True)`.

        Parameters
        ----------
        node_ids : set of int
            :prop:`netsquid.nodes.node.Node.ID` of nodes for which entanglement information is requested.
        other_nodes_allowed : bool
            True if the returned entanglement information can be about entangled states which are not just shared
            by the nodes in `node_ids`, but also by additional nodes.
            False if the returned entanglement information must describe entanglement between the nodes in `node_ids`
            and no other nodes.
        must_be_intact : bool
            True if only entanglement information for which `EntanglementInfo.intact == True` should be returned.
            An `EntanglementInfo` that is not "intact" represents an entangled state that is thought to have been
            destroyed somehow, rendering the qubit no longer entangled.

        Returns
        -------
        list of :class:`entanglement_tracker_service.EntanglementInfo`
            All available entanglement information about entangled states that include the nodes in `node_ids`.

        """
        node_ids = set(node_ids)
        if other_nodes_allowed:
            entanglement_infos = [entanglement_info for entanglement_info in self._entanglement_info
                                  if node_ids <= entanglement_info.entangled_nodes]
        else:
            entanglement_infos = [entanglement_info for entanglement_info in self._entanglement_info
                                  if node_ids == entanglement_info.entangled_nodes]
        if must_be_intact:
            return [entanglement_info for entanglement_info in entanglement_infos
                    if entanglement_info.intact]
        else:
            return entanglement_infos

    def await_entanglement(self, node_ids, link_identifiers=set(), other_nodes_allowed=False):
        """Wait for an entangled state between specific nodes based on specific elementary-link entangled states.

        Wait until the entanglement tracker believes there is an entangled state shared between the required
        nodes, that has been built using the required elementary-link entangled states.
        That is, until there is an `EntanglementInfo` available that includes `node_ids` in
        `EntanglementInfo.entangled_nodes` and `link_identifiers` in `EntanglementInfo.elementary_links`.

        In case the entanglement tracker believes that one of the required elementary-link entangled states
        is no longer part of an intact entangled state, obtaining the target entangled state is deemed impossible
        and the wait is stopped.
        That is, this method waits until either an `EntanglementInfo` is available that fulfills the conditions
        described above, or when there is an `EntanglementInfo` that includes one of the `link_identifiers`
        in `EntanglementInfo.elementary_links` for which `EntanglementInfo.intact == False`.

        To have a protocol await entanglement, put `yield from tracker.await_entanglement()` in the
        `run()` method of the protocol, where `tracker` is this entanglement tracker.

        Examples
        --------
        1. Alice and Bob are continuously generating entanglement through e.g. a repeater chain.
         Alice can consume her entanglement as soon as it becomes available by calling `await_entanglement`
         with `node_ids = {Alice.ID, Bob.ID}`.
         After the wait, she locates the entangled qubit using e.g. `get_entanglement_info_between_nodes`
         and consumes it.
         She can then again start waiting for the next entanglement.

        2. Alice created entanglement with her neighbour, which is part of a repeater chain.
         She now hopes that the repeater chain will entangle her qubit with Bob through entanglement swapping.
         However, one of the repeaters in the chain might also destroy the entangled state when a cutoff time is met.
         To this end, Alice uses `await_entanglement` with `node_ids = {Alice.ID, Bob.ID}` and
         `link_identifiers = {link_with_neighbour_identifier}`.
         After the wait is over, she checks whether her qubit is entangled with Bob, or whether the entanglement was
         destroyed (e.g. by checking the value of `get_entanglement_of_link(link_with_neithbour_identifier).intact`).
         She can then either use her entangled qubit for some application, or try again.

        Parameters
        ----------
        node_ids : set of int
            :prop:`netsquid.nodes.node.Node.ID` of the nodes that should share the entangled state that is waited for.
        link_identifiers : set of LinkIdentifier
            Identifiers of elementary-link states that should have gone into the entangled state that is waited for.
        other_nodes_allowed : bool
            True if the entangled state that is waited for can not only be shared between the nodes in `node_ids`,
            but also by additional nodes.
            False if the entangled state must be just between the nodes in `node_ids` and no other nodes.

        """
        entanglement_order = self._EntanglementOrder(node_ids=node_ids,
                                                     link_identifiers=link_identifiers,
                                                     other_nodes_allowed=other_nodes_allowed)
        self._entanglement_orders.append(entanglement_order)
        while True:
            evt = self.await_signal(sender=self, signal_label=self._signal_entanglement_order_fulfilled)
            yield evt
            fulfilled_entanglement_order = self.get_signal_by_event(event=evt.triggered_events[0], receiver=self).result
            if fulfilled_entanglement_order == entanglement_order:
                self._entanglement_orders.remove(entanglement_order)
                break

    def untrack_entanglement_info(self, entanglement_info):
        """Stop tracking a specific entangled state.

        All occurrences of the specified `EntanglementInfo` are erased from this entanglement tracker.
        If the entangled state still has alive qubits, i.e. qubits that have not been discarded or measured,
        untracking the `EntanglementInfo` is postponed until no more qubits are alive.
        The reason for this, is that an entangled state with alive qubits can still evolve (e.g. be swapped).
        If the entangled state changes before all qubits die, this method will have had no effect.

        Parameters
        ----------
        entanglement_info : :class:`entanglement_tracker_service.EntanglementInfo`
            `EntanglementInfo` that represents new knowledge about global entanglement.

        """
        if entanglement_info.has_alive_qubits:
            if entanglement_info not in self._entanglement_info_to_be_untracked:
                self._entanglement_info_to_be_untracked.append(entanglement_info)
        else:
            if entanglement_info in self._entanglement_info:
                for link_id in entanglement_info.elementary_links:
                    del self._entanglement_info_of_link[link_id]
                self._entanglement_info.remove(entanglement_info)
            if entanglement_info in self._entanglement_info_to_be_untracked:
                self._entanglement_info_to_be_untracked.remove(entanglement_info)

    def _entanglement_is_stale(self, entanglement_info):
        """Returns whether an entangled state is thought to have gone "stale".

        An entangled state is considered stale if both
        1. the entangled state has no alive qubits left (i.e. all qubits have been measured or discarded),
        which implies that the entangled state can no longer change, and
        2. the node this service runs on is not one of the nodes that are entangled by the state.

        Here, a node is still considered to be one of the entangled nodes of an entangled state even
        if the local entangled qubit has been measured out or discarded
        (see also `entanglement_tracker_service.EntanglementInfo.entangled_qubits`).
        Thus, if both qubits of a Bell state have been measured out but one of the qubits lived at the node this
        entanglement tracker runs on, the entangled state is not considered stale.

        Parameters
        ----------
        entanglement_info : :class:`entanglement_tracker_service.EntanglementInfo`
            `EntanglementInfo` that represents new knowledge about global entanglement.

        """
        return not entanglement_info.has_alive_qubits and self.node.ID not in entanglement_info.entangled_nodes

    def _process_entanglement_update(self, entanglement_info):
        """Process new entanglement information and see what this means for outstanding entanglement orders.

        It is checked whether `entanglement_info` fulfills an outstanding entanglement order
        (used to await specific entangled states, see `BellStateTracker.await_entanglement()`).
        If it does, the fulfillment is further processed.

        Parameters
        ----------
        entanglement_info : :class:`entanglement_tracker_service.EntanglementInfo`
            `EntanglementInfo` that represents new knowledge about global entanglement.

        Notes
        -----
        Every time knowledge of global entanglement is updated, `_process_entanglement_update()` should be called
        once for each `EntanglementInfo` representing new knowledge, with that `EntanglementInfo` as argument.
        For example, when a new link is registered, the corresponding elementary `EntanglementInfo` should be
        passed to this method.

        """
        for entanglement_order in self._entanglement_orders:
            if self._entanglement_info_fulfills_entanglement_order(entanglement_info, entanglement_order):
                self.send_signal(signal_label=self._signal_entanglement_order_fulfilled,
                                 result=entanglement_order)

    @staticmethod
    def _entanglement_info_fulfills_entanglement_order(entanglement_info: BellStateEntanglementInfo,
                                                       entanglement_order):
        """Determine if a specific `EntanglementInfo` fulfills a specific entanglement order.

        `entanglement_info` fulfills `entanglement_order` if either of the following is true:
        1. `entanglement_info` contains one of the elementary links specified by `entanglement_order`,
        and `entanglement_info.intact` is `False`.
        This indicates that the elementary link has been discarded and the desired entangled state can no longer
        be created.
        2. `entanglement_info` contains all elementary links and all nodes specified by `entanglement_order`.
        If `entanglement_order.other_nodes_allowed` is `False`, there can be no entangled nodes in `entanglement_info`
        except for those specified by `entanglement_order`.

        Parameters
        ----------
        entanglement_info : :class:`entanglement_tracker_service.EntanglementInfo`
            `EntanglementInfo` of which is should be determined whether it fulfills the entanglement order.
        entanglement_order : :class:`BellStateTracker._EntanglementOrder`
            Entanglement order of which is should be determined whether it is fulfilled by `entanglement_order`

        Returns
        -------
        bool
            True if `entanglement_info` fulfills `entanglement_order`, False otherwise.

        """
        # First, check if one of the required links hasn't been discarded
        entanglement_info_contains_a_required_link = \
            not entanglement_info.elementary_links.isdisjoint(entanglement_order.link_identifiers)
        if entanglement_info_contains_a_required_link and not entanglement_info.intact:
            return True
        # Second, check if the new entanglement contains all required elementary links
        if not entanglement_order.link_identifiers <= entanglement_info.elementary_links:
            return False
        # Next, check if the new entanglement contains all required nodes
        if not entanglement_order.node_ids <= entanglement_info.entangled_nodes:
            return False
        # If no extra nodes are allowed, check if exactly the required nodes are contained
        if not entanglement_order.other_nodes_allowed:
            if entanglement_order.node_ids != entanglement_info.entangled_nodes:
                return False
        # If not set to False by above, it contains both the right nodes and right links
        return True

    @property
    def routing_service(self) -> ClassicalRoutingService:
        return self.node.driver.services[ClassicalRoutingService]

    def _broadcast_request(self, request):
        """Broadcast a request so that it can be picked up by other entanglement trackers.

        Parameters
        ----------
        request : any
            The request to be broadcasted.

        """
        remote_req = RemoteServiceRequest(request=request, service=EntanglementTrackerService, origin=self.node.name,
                                          targets=[])

        self.routing_service.broadcast(remote_req)
