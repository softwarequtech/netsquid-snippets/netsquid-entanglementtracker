NetSquid-EntanglementTracker (2.0.0)
================================

Description
-----------
Snippet containing quantum-network entanglement tracker.

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Documentation
-------------

How to build the docs:

First build the docs by:

```bash
make build-docs
```

This will first install any required dependencies and build the html files.

To open the built docs (using 'open' or 'xdg-open'), do:

```bash
make open-docs
```

To both build the html files and open them, do:
```bash
make docs
```

Usage
-----

See each individual [module](https://docs.netsquid.org/snippets/netsquid-entanglementtracker/api.html) for how to use the various components and models.

Contact
-------

For questions, please contact the maintainer of this snippet: Guus Avis (g.avis[at]tudelft.nl)

Contributors
------------

 - Guus Avis

License
-------

The NetSquid-SnippetTemplate has the following license:

> Copyright 2020 QuTech (TUDelft and TNO)
>
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
>
>     http://www.apache.org/licenses/LICENSE-2.0
>
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
