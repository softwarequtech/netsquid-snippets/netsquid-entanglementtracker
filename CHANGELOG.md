CHANGELOG
=========

2024-08-27 (2.0.0)
------------------
Major update for integration with netsquid-netbuilder

- Removed entanglement_tracker_service.py, it has been moved to netsquid-driver
- BellStateTracker now uses ClassicalRoutingService to exchange information with other entanglement tracker services instead of messages.
- Added trigger of RuntimeError if overwriting an existing local link at a memory position

2022-06-21 (1.0.0)
------------------
- Exported all code related to entanglement tracking and cutoff mechanisms from private repo on different gitlab instance.
